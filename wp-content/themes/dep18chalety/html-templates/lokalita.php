<?php include('inc/head.php'); ?>
<body class="page-locality">
<?php include('inc/header.php'); ?>
<main id="main">
    <div class="el-section section-intro">
        <div class="inner">
            <div class="el-float-obj num-1"><img src="../public/i/fo-25.png" srcset="../public/i/fo-25@2x.png 2x" alt=""></div>
            <div class="block-text formated-output">
                <h1 class="el-h1">lokalita</h1>
                <div class="el-text-offset hc-text-large">
                    <p>Demänovská dolina je najnavštevovanejšou lokalitou Nízkych Tatier v srdci regiónu Liptov. Liptov je jedinečným regiónom na severnom Slovensku, je obľúbenou turistickou destináciou nielen kvôli rozmanitosti liptovskej prírody, vysokým štítom a zeleným lúkam, na ktorých vídať stáda pasúcich sa oviec ako z rozprávky o Maťkovi a Kubkovi, ale najmä kvôli skvelým podmienkam pre rozvoj cestovného ruchu.</p>
                    <p>Dolina sa nachádza južne od mesta Liptovský Mikuláš, na severných svahoch nízkotatranského horstva. Chopok, Ďumbier a Kráľová hoľa a iné pozoruhodné končiare predstavujú rozsiahly horský systém, pričom hlavný hrebeň sa ťahá v dĺžke takmer 90 km. Najvyššie z končiarov presahujú nadmorskú výšku 2 000 m n.m. Demänovská Dolina je súčasťou Národného parku Nízke Tatry, ktorý sa vyznačuje pozoruhodnými prírodnými, scenerickými skvostami ako je Vrbické pleso a Demänovský kras. Kultúrna hodnota regiónu je obrovská vďaka množstvu kúrií, skanzenov, kaštieľov, zámkov a hradných zrúcanín, ktoré určite stoja za návštevu.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-map">
        <div class="inner-wrap">
            <div class="map-wrap">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d41757.343416225674!2d19.347589222281133!3d49.14677679944373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4715a5b288071c3b%3A0x400f7d1c6970f30!2zMDM0IDgyIEzDusSNa3k!5e0!3m2!1sen!2ssk!4v1519725191228" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="el-section section-benefits">
        <div class="el-float-obj num-1"><img src="../public/i/fo-27.png" srcset="../public/i/fo-27@2x.png 2x" alt=""></div>
        <div class="inner">
            <div class="el-float-obj num-2"><img src="../public/i/fo-26.png" srcset="../public/i/fo-26@2x.png 2x" alt=""></div>
            <div class="block-text formated-output">
                <h2 class="el-h3">dostupnosť</h2>
                <div class="el-text-offset">
                    <i class="icon-arrow"></i>
                    <div class="el-cols cols-2">
                        <div class="col">
                            <ul>
                                <li>iba 150 m od strediska Jasná</li>
                                <li>iba 15 km od Liptovského Mikuláša</li>
                            </ul>
                        </div>
                        <div class="col">
                            <ul>
                                <li>1 hodinu autom od letiska v Poprade</li>
                                <li>3 hodiny od letiska v Krakove</li>
                                <li>3,5 hodiny od letiska v Bratislave</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-item-1">
        <div class="inner">
            <div class="el-float-obj num-1"><img src="../public/i/fo-28.png" srcset="../public/i/fo-28@2x.png 2x" alt=""></div>
            <div class="block-halves image-left">
                <div class="text-wrap formated-output">
                    <h2 class="el-h2">lyžovanie</h2>
                    <div class="el-text-offset">
                        <p>Pod majestátnym Chopkom je predsa najlepšia lyžovačka na Slovensku! Rozsiahla sieť lyžiarskych zjazdoviek prináša skvelý športový zážitok. Trate sú neustále povrchovo upravované, aby bol zabezpečený maximálne kvalitný zjazd ako pre začiatočníka, pokročilého lyžiara i pre profesionálnych športovcov.</p>
                        <p>Rodiny s deťmi tu nájdu možnosti pre prvé zjazdy na snehu vďaka sieti modrých zjazdoviek a širokej ponuky lyžiarskych škôl a aktivít. Stredisko Ski Jasná Chopok - Nízke Tatry ponúka cca 46 km zjazdoviek, 30 vlekov a lanoviek 8 freeridových zón, večerné lyžovanie.</p>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/slide03.jpg" srcset="../public/i/slide03.jpg 2x">
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-item-2">
        <div class="inner">
            <div class="block-halves image-right">
                <div class="text-wrap formated-output">
                    <h2 class="el-h2">príroda</h2>
                    <div class="el-text-offset">
                        <p>Demänovská Dolina ponúka rozmanitú sieť turistických chodníčkov i cyklochodníkov priamo v
                            lone nízkotatranskej prírody.</p>
                        <p>Každá z týchto cestičiek vám poskytne dokonalý zážitok z turistiky a z nádherných
                            vysokohorských výhľadov. V každom ročnom období máte ako turista na výber množstvo trás,
                            ktoré vás zavedú na tie najkrajšie a najobľúbenejšie miesta, na druhej strane i na miesta,
                            ktoré ešte stále čakajú na objavenie.</p>
                        <p>Trasy je možné si vybrať podľa náročnosti. Takže sa zhlboka nadýchnite a hor sa za čerstvým
                            tatranským vzduchom, jedinečnou atmosférou a prekrásnymi výhľadmi.</p>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/slide04.jpg" srcset="../public/i/slide04.jpg 2x">
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-item-3">
        <div class="inner">
            <div class="el-float-obj num-1"><img src="../public/i/fo-29.png" srcset="../public/i/fo-29@2x.png 2x" alt=""></div>
            <div class="block-halves image-left">
                <div class="text-wrap formated-output">
                    <h2 class="el-h2">výlety</h2>
                    <div class="el-text-offset">
                        <p>Lokalita ponúka množstvo príležitostí na spoznávanie prírodných predností regiónu ako
                            napríklad jaskyne, vodné nádrže, horské plesá, či iné trávenie voľného času napríklad
                            v Aquaparku Tatralandia.</p>
                        <p>Pre milovníkov adrenalínu je k dispozícii okrem iného Hurricane Factory Tatralandia -
                            aerodynamický tunel, horská bobová dráha Žiarce, Adrenalín Centrum Liptovský Mikuláš, ktorý
                            ponúka rafting na divokej vode, paintball, paragliding, jazdu na štvorkolkách, lukostreľbu,
                            airsoft. Lokalita na jednej strane ponúka bohatý balík aktivít pre rodiny s deťmi, na druhej
                            strane si na svoje prídu i skupinky mladých, ktorí vyhľadávajú večernú zábavu.</p>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/slide05.jpg" srcset="../public/i/slide05.jpg 2x">
                </div>
            </div>
        </div>
    </div>
    <?php include('inc/section-contact.php'); ?>
</main>
<?php include('inc/footer.php'); ?>
<?php include('inc/foot.php'); ?>