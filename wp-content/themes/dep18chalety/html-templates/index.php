<?php include('inc/head.php'); ?>
<body class="page-front">
<?php include('inc/landing.php'); ?>
<?php include('inc/header.php'); ?>
<main id="main">
    <div class="el-section section-slider">
        <div class="el-float-obj num-1"><img src="../public/i/fo-01.png" srcset="../public/i/fo-01@2x.png 2x" alt=""></div>
        <div class="el-float-obj num-3"><img src="../public/i/fo-03.png" srcset="../public/i/fo-03@2x.png 2x" alt=""></div>
        <div class="inner">
            <div class="el-float-obj num-2"><img src="../public/i/fo-02.png" srcset="../public/i/fo-02@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-4"><img src="../public/i/fo-04.png" srcset="../public/i/fo-04@2x.png 2x" alt=""></div>
            <div class="el-slider">
                <div class="badge">
                    <div class="inner">
                        <p class="hc-color-black">aktuálne<br>prebieha:</p><p class="hc-color-white"><strong>1. fáza<br>výstavby</strong></p>
                    </div>
                </div>
                <div class="items">
                    <div class="item"><a href="/" class="img-bg" style="background-image:url('../public/i/slide01.png');"></a></div>
                    <div class="item"><a href="/" class="img-bg" style="background-image:url('../public/i/slide02.png');"></a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-why">
        <div class="inner">
            <div class="el-float-obj num-1"><img src="../public/i/fo-05.png" srcset="../public/i/fo-05@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-2"><img src="../public/i/fo-06.png" srcset="../public/i/fo-06@2x.png 2x" alt=""></div>
            <div class="block-text">
                <h2 class="block-title el-h3">prečo si vybrať CHALETY LÚČKY? <br><span class="hc-text-regular hc-color-secondary">INVESTÍCIA DO NEHNUTEĽNOSTÍ</span></h2>
                <div class="el-text-offset">
                    <i class="icon-arrow"></i>
                    <div class="el-cols">
                        <div class="col formated-output">
                            <h4 class="el-h4">SKVELÁ LOKALITA APARTMÁNOV</h4>
                            <p>Výhodnú polohu v známom stredisku Jasná ocenia nielen lyžiari, ale i milovníci turistiky keďže priamo z apartmánov má možnosť každý turista vyraziť na turistické chodníky a obdivovať krásy nízkotatranskej prírody.</p>
                        </div>
                        <div class="col formated-output">
                            <h4 class="el-h4">vysoký záujem o ubytovanie počas celého roka</h4>
                            <p>Získajte jeden alebo niekoľko apartmánov z našej ponuky a využívajte ho sami vo svojom voľnom čase a v prípade záujmu ho vyťažujte i inými návštevníkmi, čím budete v plnej miere profitovať z vlastníctva apartmánu.</p>
                        </div>
                        <div class="col formated-output">
                            <h4 class="el-h4">výborná cena ponúkaných apartmánov</h4>
                            <p>Luxusné apartmány v rámci šiestich chaletov poskytujú nadštandardné ubytovanie v srdci Nízkych Tatier za výbornú cenu, za ktorú dostanete svoj vysnívaný dovolenkový apartmán podľa vlastných preferencií.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-about">
        <div class="inner">
            <div class="el-float-obj num-1"><img src="../public/i/fo-07.png" srcset="../public/i/fo-07@2x.png 2x" alt=""></div>
            <div class="block-halves image-right">
                <div class="text-wrap formated-output">
                    <h3 class="el-h2">o projekte</h3>
                    <div class="el-text-offset">
                        <i class="icon-cottage"></i>
                        <h4 class="el-h4">LUXUSNÉ UBYTOVANIE V HORSKOM PROSTREDÍ</h4>
                        <p>Komplex šiestich chaletov Lúčky predstavuje ideálne miesto na oddych pre každého, kto rád trávi voľné chvíle v prírode v náručí hôr.</p>
                        <p>Chaletové apartmány svojou polohou uprostred obľúbenej Demänovskej Doliny s úžasnými výhľadmi na končiare Nízkych Tatier bezpochyby zaručia neopísateľný pocit dokonalého oddychu a pohodlia v každom ročnom období. Vyberte si preto z nami ponúkaných voľných apartmánov a trávte nezabudnuteľné chvíle oddychu vo vlastnom útulnom chalete.</p>
                        <a href="/" class="el-btn">zistiť viac</a>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/img01.png" srcset="../public/i/img01.png 2x">
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-gallery">
        <div class="el-float-obj num-1"><img src="../public/i/fo-09.png" srcset="../public/i/fo-09@2x.png 2x" alt=""></div>
        <div class="el-float-obj num-2"><img src="../public/i/fo-10.png" srcset="../public/i/fo-10@2x.png 2x" alt=""></div>
        <div class="el-float-obj num-3"><img src="../public/i/fo-12.png" srcset="../public/i/fo-12@2x.png 2x" alt=""></div>
        <div class="el-float-obj num-4"><img src="../public/i/fo-13.png" srcset="../public/i/fo-13@2x.png 2x" alt=""></div>
        <div class="inner">
            <div class="el-float-obj num-5"><img src="../public/i/fo-08.png" srcset="../public/i/fo-08@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-6"><img src="../public/i/fo-11.png" srcset="../public/i/fo-11@2x.png 2x" alt=""></div>
            <div class="tabs-wrap">
                <div class="block-text">
                    <h2 class="section-title el-h2 hc-color-primary">galéria</h2>
                    <ul class="section-tabs">
                        <li class="tab"><a href="#gallery-1">interiér</a></li>
                        <li class="tab"><a href="#gallery-2">exteriér</a></li>
                        <li class="tab"><a href="#gallery-3">lokalita</a></li>
                    </ul>
                </div>
                <div class="tab-panel" id="gallery-1">
                    <div class="el-gallery">
                        <div class="items">
                            <div class="item"><a href="http://chalety-jasna.localhost/wp-content/themes/my-theme/public/i/slide01.png" class="img-bg" style="background-image:url('../public/i/slide01.png');"></a></div>
                            <div class="item"><a href="http://chalety-jasna.localhost/wp-content/themes/my-theme/public/i/slide02.png" class="img-bg" style="background-image:url('../public/i/slide02.png');"></a></div>
                            <div class="item"><a href="http://chalety-jasna.localhost/wp-content/themes/my-theme/public/i/slide03.jpg" class="img-bg" style="background-image:url('../public/i/slide03.jpg');"></a></div>
                            <div class="item"><a href="http://chalety-jasna.localhost/wp-content/themes/my-theme/public/i/slide04.jpg" class="img-bg" style="background-image:url('../public/i/slide04.jpg');"></a></div>
                            <div class="item"><a href="http://chalety-jasna.localhost/wp-content/themes/my-theme/public/i/slide05.jpg" class="img-bg" style="background-image:url('../public/i/slide05.jpg');"></a></div>
                        </div>
                        <div class="thumbnails">
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide01.png');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide02.png');"></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide03.jpg');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide04.jpg');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide05.jpg');"></span></span></div>
                        </div>
                    </div>
                </div>
                <div class="tab-panel" id="gallery-2">
                    <div class="el-gallery">
                        <div class="items">
                            <div class="item"><a href="../public/i/slide01.png" class="img-bg" style="background-image:url('../public/i/slide01.png');"></a></div>
                            <div class="item"><a href="../public/i/slide02.png" class="img-bg" style="background-image:url('../public/i/slide02.png');"></a></div>
                            <div class="item"><a href="../public/i/slide03.jpg" class="img-bg" style="background-image:url('../public/i/slide03.jpg');"></a></div>
                            <div class="item"><a href="../public/i/slide04.jpg" class="img-bg" style="background-image:url('../public/i/slide04.jpg');"></a></div>
                            <div class="item"><a href="../public/i/slide05.jpg" class="img-bg" style="background-image:url('../public/i/slide05.jpg');"></a></div>
                        </div>
                        <div class="thumbnails">
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide01.png');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide02.png');"></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide03.jpg');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide04.jpg');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide05.jpg');"></span></span></div>
                        </div>
                    </div>
                </div>
                <div class="tab-panel" id="gallery-3">
                    <div class="el-gallery">
                        <div class="items">
                            <div class="item"><a href="../public/i/slide01.png" class="img-bg" style="background-image:url('../public/i/slide01.png');"></a></div>
                            <div class="item"><a href="../public/i/slide02.png" class="img-bg" style="background-image:url('../public/i/slide02.png');"></a></div>
                            <div class="item"><a href="../public/i/slide03.jpg" class="img-bg" style="background-image:url('../public/i/slide03.jpg');"></a></div>
                            <div class="item"><a href="../public/i/slide04.jpg" class="img-bg" style="background-image:url('../public/i/slide04.jpg');"></a></div>
                            <div class="item"><a href="../public/i/slide05.jpg" class="img-bg" style="background-image:url('../public/i/slide05.jpg');"></a></div>
                        </div>
                        <div class="thumbnails">
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide01.png');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide02.png');"></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide03.jpg');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide04.jpg');"></span></span></div>
                            <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('../public/i/slide05.jpg');"></span></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-locality">
        <div class="el-float-obj num-1"><img src="../public/i/fo-14.png" srcset="../public/i/fo-14@2x.png 2x" alt=""></div>
        <div class="el-float-obj num-2"><img src="../public/i/fo-15.png" srcset="../public/i/fo-15@2x.png 2x" alt=""></div>
        <div class="inner">
            <div class="el-float-obj num-3"><img src="../public/i/fo-16.png" srcset="../public/i/fo-16@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-4"><img src="../public/i/fo-17.png" srcset="../public/i/fo-17@2x.png 2x" alt=""></div>
            <div class="block-halves image-right">
                <div class="text-wrap formated-output">
                    <h3 class="el-h2 hc-color-primary">lokalita</h3>
                    <div class="el-text-offset">
                        <i class="icon-tree2"></i>
                        <h4 class="el-h4">krásne prostredie nízkych tatier</h4>
                        <p>Demänovská Dolina v srdci Nízkych Tatier je vyhľadávaným miestom vyznávačov zimných športov a turistických nadšencov.</p>
                        <p>Na lyžiarov tu čaká jedno z najväčších lyžiarskych stredísk na Slovensku s množstvom rôzne náročných lyžiarskych terénov. Každý lyžiar si určite vyberie správnu trasu vzhľadom na svoje schopnosti a zažije príjemnú lyžovačku. Obdobie leta praje turistike a objavovaniu nízkotatranskej prírody. Región navyše ponúka množstvo iných voľnočasových aktivít, ktorými návštevník dokonale využije voľný čas.</p>
                        <a href="/" class="el-btn">viac o lokalite</a>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/img02.png" srcset="../public/i/img02.png 2x" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-visualization">
        <div class="inner">
            <div class="el-float-obj num-1"><img src="../public/i/fo-18.png" srcset="../public/i/fo-18@2x.png 2x" alt=""></div>
            <div class="block-text">
                <h2 class="section-title el-h2 hc-color-primary">3d <span class="hc-text-light">vizualizácia</span></h2>
            </div>
            <?php include('inc/block-visualization.php'); ?>
        </div>
    </div>
    <div class="el-section section-developer">
        <div class="el-float-obj num-1"><img src="../public/i/fo-19.png" srcset="../public/i/fo-19@2x.png 2x" alt=""></div>
        <div class="inner">
            <div class="el-float-obj num-2"><img src="../public/i/fo-20.png" srcset="../public/i/fo-20@2x.png 2x" alt=""></div>
            <div class="block-text">
                <h2 class="section-title el-h2 hc-color-primary">developer</h2>
            </div>
            <div class="block-halves image-right">
                <div class="text-wrap formated-output">
                    <div class="el-text-offset">
                        <i class="icon-tree1"></i>
                        <h4 class="el-h4">NAŠE SKÚSENOSTI<br><span class="hc-color-secondary">ZA KAŽDÝM PROJEKTOM STOJÍ VŽDY SILNÝ INVESTOR</span></h4>
                        <p>Naša spoločnosť sa venuje investičným projektom v oblasti cestovného ruchu, a to predovšetkým za účelom poskytovania ubytovacích kapacít v najviac navštevovaných lokalitách Slovenska. Priamo v Demänovskej Doline má skúsenosti predovšetkým s výstavbou a prevádzkou penziónu Energetik, ktorý je návštevníkom známy z tejto lokality.</p>
                        <a href="/" class="el-btn">viac o developerovi</a>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/img04.png" srcset="../public/i/img04@2x.png 2x" alt="">
                </div>
            </div>
        </div>
    </div>
    <?php include('inc/section-contact.php'); ?>
</main>
<?php include('inc/footer.php'); ?>
<?php include('inc/foot.php'); ?>