<?php include('inc/head.php'); ?>
    <body class="page-apartman">
<?php include('inc/header.php'); ?>
    <main id="main">
        <div class="el-section section-intro">
            <div class="el-float-obj num-1"><img src="../public/i/fo-26.png" srcset="../public/i/fo-26@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2"><img src="../public/i/fo-17.png" srcset="../public/i/fo-17@2x.png 2x" alt=""></div>
                <div class="block-text formated-output">
                    <h1 class="el-h1">detail apartmánu</h1>
                    <div class="el-text-offset hc-text-large">
                        <p>Luxusné apartmány ponúkame dokončené na kľúč s kompletne zariadenými interiérmi podľa
                            vizualizácií. Všetko v nadštandardnom prevedení a s použitím najkvalitnejších materiálov,
                            vrátane spotrebičov, sanity a doplnkov, pripravené na okamžité užívanie. V priestoroch
                            apartmánov budú použité predovšetkým prírodné materiály – to sa týka podláh,  obkladov stien
                            a stropov vo všetkých miestnostiach okrem dizajnovej kúpeľne a toaliet. V kuchynskej časti
                            bude stena upravená obkladom s kamenným efektom, rovnako aj časť stien v obývacej časti.
                            Nábytok bude ladený do nenápadných farieb v svetlých odtieňoch, vstavané skrine a kuchynka
                            vyhotovená z prírodnej dyhy. Celý priestor bude úplne presvetlený cez nadrozmerné okná
                            a terasu.</p>
                        <p>V súčasnosti sú k dispozícii vizualizácie chaletov a apartmánov, po postupnom dokončovaní je
                            možné dohodnúť sa na obhliadke a vyskúšať si svoj budúci apartmán.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-visualization">
            <div class="el-float-obj num-1"><img src="../public/i/fo-29.png" srcset="../public/i/fo-29@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2"><img src="../public/i/fo-36.png" srcset="../public/i/fo-36@2x.png 2x" alt=""></div>
                <div class="el-float-obj num-3"><img src="../public/i/fo-16.png" srcset="../public/i/fo-16@2x.png 2x" alt=""></div>
                <div class="el-float-obj num-4"><img src="../public/i/fo-08.png" srcset="../public/i/fo-081@2x.png 2x" alt=""></div>
                <h2 class="section-title el-h2 hc-color-primary"><span class="hc-text-regular">chalet</span> č.5</h2>
                <?php include('inc/block-visualization.php'); ?>
                <div class="block-plan">
                    <a name="podorys"></a>
                    <div class="plan-wrap active" data-id="chalet_1_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/1.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 1</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>1</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. jednoizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>44.17 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>31.87 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>4.19 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                                <li><a href="/" class="button">STAV: <strong>predaný</strong></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap active" data-id="chalet_1_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/2.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 1</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>2</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>84.84 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.64 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.81 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                                <li><a href="/" class="button">STAV: <strong>voľný</strong></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_1_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/3.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 1</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>3</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. dvojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>52.56 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>26.35 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_1_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/4.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 1</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>4</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>84.81 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.63 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_1_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/5.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 1</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>5</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap.    dvojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>43.43 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>25.33 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_1_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/6.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 1</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>6</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>84.45 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.27 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_2_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/1.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 2</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>1</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. jednoizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>44.17 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>31.87 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>4.19 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_2_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/2.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 2</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>2</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>84.84 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.64 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.81 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_2_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/3.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 2</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>3</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. dvojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>52.56 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>26.35 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_2_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/4.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 2</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>4</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>84.81 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.63 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_2_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/5.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 2</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>5</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap.    dvojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>43.43 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>25.33 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_2_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/6.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 2</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>6</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>84.45 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.27 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_3_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/1.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 3</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>1</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. jednoizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>44.17 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>31.87 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>4.19 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_3_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/2.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 3</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>2</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>84.84 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.64 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.81 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_3_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/3.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 3</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>3</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. dvojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>52.56 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>26.35 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_3_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/4.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 3</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>4</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>84.81 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.63 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_3_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/5.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 3</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>5</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap.    dvojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>43.43 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>25.33 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_3_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/6.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 3</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>6</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>84.45 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.27 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_4_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/1.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 4</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>1</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. jednoizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>44.17 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>31.87 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>4.19 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_4_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/2.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 4</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>2</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>84.84 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.64 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.81 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_4_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/3.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 4</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>3</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. dvojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>52.56 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>26.35 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_4_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/4.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 4</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>4</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>84.81 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.63 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_4_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/5.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 4</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>5</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap.    dvojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>43.43 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>25.33 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_4_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/6.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 4</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>6</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>84.45 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.27 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_5_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/1.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 5</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>1</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. jednoizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>44.17 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>31.87 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>4.19 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_5_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/2.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 5</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>2</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>84.84 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.64 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.81 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_5_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/3.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 5</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>3</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. dvojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>52.56 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>26.35 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_5_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/4.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 5</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>4</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>84.81 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.63 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_5_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/5.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 5</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>5</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap.    dvojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>43.43 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>25.33 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_5_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/6.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 5</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>6</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>84.45 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.27 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_6_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/1.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 6</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>1</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. jednoizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>44.17 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>31.87 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>4.19 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_6_1">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/2.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 6</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>2</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>prízemie</strong></li>
                                <li>Výmera: <strong>84.84 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.64 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.81 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_6_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/3.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 6</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>3</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap. dvojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>52.56 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>8.11 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>26.35 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                                <li>Orientácia terasy: <strong>SV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_6_2">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/4.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 6</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>4</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>1. podlažie</strong></li>
                                <li>Výmera: <strong>84.81 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.63 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_6_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/5.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 6</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>5</strong></div></li>
                                <li>Typ: <strong>2-lôžkový ap.    dvojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>43.43 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>25.33 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>12.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.61 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SZ</strong></li>
                            </ul>
                        </div>
                    </div>
                    <div class="plan-wrap" data-id="chalet_6_3">
                        <div class="image-wrap">
                            <img src="http://chaletylucky.sk/wp-content/uploads/2018/03/6.png">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3">Chalet <strong>č. 6</strong></h3>
                            <ul class="params">
                                <li><div class="apartment-label">Apartmán : <strong>6</strong></div></li>
                                <li>Typ: <strong>4-lôžkový ap.  trojizbový</strong></li>
                                <li>Poschodie: <strong>podkrovie</strong></li>
                                <li>Výmera: <strong>84.45 m<sup>2</sup></strong></li>
                                <li>Z toho terasa: <strong>21.9 m<sup>2</sup></strong></li>
                                <li>Obytný priestor: <strong>27.27 m<sup>2</sup></strong></li>
                                <li>Spálňa samostatná: <strong>29.49 m<sup>2</sup></strong></li>
                                <li>Hygienické zázemie: <strong>5.79 m<sup>2</sup></strong></li>
                                <li>Orientácia spálne: <strong>SV</strong></li>
                                <li>Orientácia terasy: <strong>JV</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--<div class="block-plan">
                    <div class="plan-wrap">
                        <div class="image-wrap">
                            <img src="../public/i/plan-01.png" srcset="../public/i/plan-01@2x.png 2x">
                        </div>
                        <div class="text-wrap">
                            <h3 class="title el-h3"><span class="hc-text-regular">chalet</span> č.5</h3>
                            <ul class="params">
                                <li>Apartmán: <strong>č. 16</strong></li>
                                <li>Poschodie: <strong>druhé</strong></li>
                                <li>Rozloha apartmánu <strong>120 m<sup>2</sup></strong></li>
                                <li>Rozloha terasy: <strong>27 m<sup>2</sup></strong></li>
                                <li>Celková prenajímateľná plocha: <strong>147 m<sup>2</sup></strong></li>
                            </ul>
                            <ul class="desc">
                                <li><span class="el-legend-color color-1"></span><span>Prenajímateľná plocha</span></li>
                                <li><span class="el-legend-color color-2"></span><span>Hygienické zázemie</span></li>
                                <li><span class="el-legend-color color-3"></span><span>Spoločné priestory</span></li>
                                <li><span class="el-legend-color color-4"></span><span>Vertikálne komunikácie</span></li>
                                <li><span class="el-legend-color color-5"></span><span>Terasa</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="legend-wrap">
                        <h3 class="title">Legenda</h3>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-1"></span><span>Prenajímateľná plocha</span></div>
                            <div class="legend-col"><span>323,12m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-2"></span><span>Hygienické zázemie</span></div>
                            <div class="legend-col"><span>26,44m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-3"></span><span>Spoločné priestory</span></div>
                            <div class="legend-col"><span>15,97m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-4"></span><span>Vertikálne komunikácie</span></div>
                            <div class="legend-col"><span>42,37m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-5"></span><span>Terasa</span></div>
                            <div class="legend-col"><span>117,80m<sup>2</sup></span></div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
        <?php include('inc/section-contact.php'); ?>
    </main>
<?php include('inc/footer.php'); ?>
<?php include('inc/foot.php'); ?>