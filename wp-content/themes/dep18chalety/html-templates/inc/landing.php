<div id="landing">
    <div class="el-float-obj num-1"><img src="../public/i/fo-landing-01.png" srcset="../public/i/fo-landing-01@2x.png 2x" alt=""></div>
    <div class="el-float-obj num-2"><img src="../public/i/fo-landing-02.png" srcset="../public/i/fo-landing-02@2x.png 2x" alt=""></div>
    <div class="el-float-obj num-3"><img src="../public/i/fo-landing-03.png" srcset="../public/i/fo-landing-03@2x.png 2x" alt=""></div>
    <div class="el-float-obj num-4"><img src="../public/i/fo-landing-04.png" srcset="../public/i/fo-landing-04@2x.png 2x" alt=""></div>
    <div class="el-float-obj num-5"><img src="../public/i/fo-landing-05.png" srcset="../public/i/fo-landing-05@2x.png 2x" alt=""></div>
    <div class="el-float-obj num-6"><img src="../public/i/fo-landing-06.png" srcset="../public/i/fo-landing-06@2x.png 2x" alt=""></div>
    <div class="el-float-obj num-7"><img src="../public/i/fo-landing-07.png" srcset="../public/i/fo-landing-07@2x.png 2x" alt=""></div>
    <div class="inner">
        <h2 class="landing-subtitle">apartmánový developerský projekt <br><span class="hc-text-regular">na predaj</span></h2>
        <h1 class="landing-title"><span class="hc-color-white">Chalety</span><br>lúčky</h1>
    </div>
</div>