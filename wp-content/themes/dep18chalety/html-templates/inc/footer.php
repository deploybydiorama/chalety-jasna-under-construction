<footer id="footer">
    <div class="el-float-obj num-1"><img src="../public/i/fo-22.png" srcset="../public/i/fo-22@2x.png 2x" alt=""></div>
    <div class="inner">
        <div class="footer-cols">
            <div class="footer-col">
                <h3 class="footer-title">Navigácia:</h3>
                <ul class="footer-menu">
                    <li><a href="/">Projekt</a></li>
                    <li><a href="/">Lokalita</a></li>
                    <li><a href="/">Galéria</a></li>
                    <li><a href="/">3D vizualizácia</a></li>
                    <li><a href="/">Developer</a></li>
                    <li><a href="/">Kontakt</a></li>
                </ul>
            </div>
            <div class="footer-col">
                <h3 class="footer-title">Navigácia:</h3>
                <ul class="footer-social">
                    <li><a href="/"><i class="icon-facebook"></i></a></li>
                    <li><a href="/"><i class="icon-instagram"></i></a></li>
                    <li><a href="/"><i class="icon-youtube"></i></a></li>
                </ul>
            </div>
            <div class="footer-col">
                <h3 class="footer-title">Investor:</h3>
                <div class="footer-text formated-output">
                    <p>AIRAVATA HOLDING s.r.o. Pribinova 25<br>811 09 Bratislava<br><a href="mailto:info@airavata.sk">info@airavata.sk</a><br><a href="tel:+421 2 11 22 33 44">+421 2 11 22 33 44</a></p>
                </div>
            </div>
            <div class="footer-col">
                <h3 class="footer-title">Autori projektu:</h3>
                <ul class="footer-menu">
                    <li><a href="/">Airavata</a></li>
                    <li><a href="/">Stavebná spoločnosť</a></li>
                    <li><a href="/">Architekti</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>