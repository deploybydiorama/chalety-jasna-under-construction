<!DOCTYPE html>
<html>
<head>
    <!-- META -->
    <title>Chalety Lúčky | Predaj apartmánov v srdci prírody</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta name="description" content="Chalety Lúčky - Predaj apartmánov v srdci prírody" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="https://www.diorama.sk/" />
    <link rel="icon" href="../public/i/favico.png" type="image/png" />

    <link rel="stylesheet" type="text/css" href="../public/css/main.min.css" />

    <script src="../bower_components/jquery/dist/jquery.js"></script>
    <script src="../bower_components/underscore/underscore.js"></script>
    <script src="../bower_components/slick-carousel/slick/slick.js"></script>
    <script src="../assets/js/plugins/jquery.magnific-popup.min.js"></script>
    <script src="../assets/js/plugins/jquery.easytabs.min.js"></script>
    <script src="../assets/js/main/script.js"></script>
</head>