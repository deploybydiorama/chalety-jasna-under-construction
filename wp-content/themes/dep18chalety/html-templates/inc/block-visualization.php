<div class="block-visualization">
    <div class="bg"><img src="../public/i/img03.png" srcset="../public/i/img03.png 2x"></div>
    <div class="desc-wrap" style="top:20%;left:50%">
        <h4 class="title">Chalet 5</h4>
        <ul class="params">
            <li>apartmán č.: 16</li>
            <li>poschodie: 2</li>
            <li>rozloha: 120m<sup>2</sup></li>
            <li>terasa: 27m<sup>2</sup></li>
        </ul>
        <a href="/" class="btn">voľný</a>
    </div>
</div>