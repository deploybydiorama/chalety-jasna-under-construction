<header id="header">
    <div class="inner">
        <a href="/" class="logo"><span class="logo-title"><span class="hc-color-white">Chalety</span><br>lúčky</span><br><span class="logo-slogan"><span class="slogan-first">predaj<br>apartmánov</span><br><span class="slogan-second">v srdci prírody</span></span></a>
        <a class="nav-toggle"><span class="burger"></span></a>
        <nav class="header-nav">
            <ul class="header-menu">
                <li><a href="projekt.php" class="active">projekt</a></li>
                <li><a href="/">Galéria</a></li>
                <li><a href="lokalita.php">Lokalita</a></li>
                <li><a href="apartman.php">3D vizualizácia</a></li>
                <li><a href="developer.php">Developer</a></li>
                <li><a href="/">Kontakt</a></li>
            </ul>
            <ul class="header-social">
                <li><a href="/"><i class="icon-facebook"></i></a></li>
                <li><a href="/"><i class="icon-instagram"></i></a></li>
                <li><a href="/"><i class="icon-phone"></i></a></li>
            </ul>
        </nav>
    </div>
</header>