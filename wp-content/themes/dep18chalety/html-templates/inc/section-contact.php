<div class="el-section section-contact">
    <div class="inner">
        <div class="el-float-obj num-1"><img src="../public/i/fo-21.png" srcset="../public/i/fo-21@2x.png 2x" alt=""></div>
        <div class="block-text">
            <h2 class="section-title el-h2">zaujala vás naša ponuka?</h2>
            <a href="tel:0211223344" class="cta"><i class="icon-phone"></i> volajte na 02/11 22 33 44</a>
        </div>
    </div>
</div>