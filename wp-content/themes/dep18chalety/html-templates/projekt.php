<?php include('inc/head.php'); ?>
<body class="page-project">
<?php include('inc/header.php'); ?>
<main id="main">
    <div class="el-section section-intro">
        <div class="el-float-obj num-1"><img src="../public/i/fo-23.png" srcset="../public/i/fo-23@2x.png 2x" alt=""></div>
        <div class="inner">
            <div class="el-float-obj num-2"><img src="../public/i/fo-20.png" srcset="../public/i/fo-20@2x.png 2x" alt=""></div>
            <div class="block-text formated-output">
                <h1 class="el-h1">o projekte</h1>
                <div class="el-text-offset hc-text-large">
                    <p>Chaletový komplex Lúčky sa nachádza v Demänovskej Doline priamo uprostred očarujúcej  nízkotatranskej prírody. Projekt Chalety Lúčky je určený pre všetkých, ktorí svoj voľný čas radi trávia v horách. Ak patríte do tejto skupiny, vyberte si preto aj Vy z našej ponuky štýlovo zariadených apartmánov a rozhodnite sa investovať do vlastného miesta na príjemný oddych.</p>
                    <p>Zažite neopísateľný pocit a atmosféru ticha v nízkotatranskej prírode, zvuk praskajúceho ohňa v krbe, šálky voňavého čaju vo svojom vlastnom apartmáne alebo relax vo wellness centre po celodennej vyčerpávajúcej lyžovačke v skvelom prostredí obľúbeného lyžiarskeho strediska. Nízke Tatry vám dovolia nazrieť do svojich najkrajších zákutí či už sa vyberiete na turistické chodníky na vlastných nohách alebo na bicykli. Takže sa stačí len zhlboka nadýchnuť, vybrať sa za čerstvým nízkotatranským vzduchom, výškami s prekrásnymi výhľadmi a príjemne unavení sa vrátiť do svojho vlastného útulného apartmánu.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-slider">
        <div class="inner">
            <div class="el-float-obj num-1"><img src="../public/i/fo-24.png" srcset="../public/i/fo-24@2x.png 2x" alt=""></div>
            <div class="el-slider">
                <div class="items">
                    <div class="item"><a href="/" class="img-bg" style="background-image:url('../public/i/slide01.png');"></a></div>
                    <div class="item"><a href="/" class="img-bg" style="background-image:url('../public/i/slide02.png');"></a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-benefits">
        <div class="el-float-obj num-1"><img src="../public/i/fo-19.png" srcset="../public/i/fo-19@2x.png 2x" alt=""></div>
        <div class="inner">
            <div class="block-text">
                <h2 class="block-title el-h3">očarujúce miesto pre oddych <br><span class="hc-text-regular hc-color-secondary">takto môže vyzerať oddych <br>vo vašom vlastnom chalete</span></h2>
                <div class="el-text-offset">
                    <i class="icon-arrow"></i>
                    <div class="el-cols">
                        <div class="col formated-output">
                            <h4 class="el-h4">krásne a <br>romanticky</h4>
                            <p>Zažite vo svojom apartmáne romantické chvíle vo dvojici.</p>
                        </div>
                        <div class="col formated-output">
                            <h4 class="el-h4">pohodlne a <br>rodinne</h4>
                            <p>Strávte dovolenku so svojou rodinou v náručí Nízkych Tatier.</p>
                        </div>
                        <div class="col formated-output">
                            <h4 class="el-h4">luxusne a zároveň tradične</h4>
                            <p>Užívajte si pobyt v luxusnom apartmáne, ktorý vonia drevom.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-item-1">
        <div class="inner">
            <div class="block-halves image-left">
                <div class="text-wrap formated-output">
                    <h2 class="el-h2">chalety</h2>
                    <div class="el-text-offset">
                        <p>Chalety Lúčky tvorí komplex šiestich luxusných chaletov, ktoré svojim vzhľadom dokonale zapadajú do prostredia. Chalety budú zaručene obľúbeným miestom pre páry, väčšie apartmány budú vhodné najmä pre rodiny s deťmi. Pri každom chalete sa nachádza parkovisko.</p>
                        <p>Chalet na pohľad bude príjemnou súčasťou priestoru, jeho povrch je riešený obkladom z kameňa a dreva, čo dodáva objektom ten správny horský ráz.</p>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/img01.png" srcset="../public/i/img01.png 2x">
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-item-2">
        <div class="inner">
            <div class="block-halves image-right">
                <div class="text-wrap formated-output">
                    <h2 class="el-h2">RELAX</h2>
                    <div class="el-text-offset">
                        <p>V areáli bude k dispozícii novovybudované wellness centrum so saunami, hygienickým zázemím a vírivkou v rámci penziónu Energetik.</p>
                        <p>Návštevník bude mať príležitosť zveriť sa do rúk kvalifikovaného maséra a užiť si dokonalý relax. Následne bude príjemným miestom na oddych odpočívareň a drevená terasa, ktorá zaručí tú najautentickejšiu formu zážitku. Penzión ponúka taktiež služby reštaurácie, ktorá návštevníkom vždy ponúkne gastronomický pôžitok.</p>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/img01.png" srcset="../public/i/img01.png 2x">
                </div>
            </div>
        </div>
    </div>
    <div class="el-section section-item-3">
        <div class="inner">
            <div class="block-halves image-left">
                <div class="text-wrap formated-output">
                    <h2 class="el-h2">apartmány</h2>
                    <div class="el-text-offset">
                        <p>V každom z chaletov sa nachádza 6 apartmánov, na každom podlaží dva – dvojlôžkové a štvorlôžkové. Luxusné apartmány pozostávajú z obývacej izby spojenej s kuchyňou,  s jednou alebo dvoma oddelenými dvojlôžkovými spálňami a terasou, z ktorej je impozantný výhľad na okolitú prírodu Demänovskej Doliny.</p>
                        <p>Celý priestor je ladený do prírodných farieb s maximálnym využitím dreva v interiéri pre dokonalý dojem.</p>
                    </div>
                </div>
                <div class="image-wrap">
                    <img src="../public/i/img01.png" srcset="../public/i/img01.png 2x">
                </div>
            </div>
        </div>
    </div>
    <?php include('inc/section-contact.php'); ?>
</main>
<?php include('inc/footer.php'); ?>
<?php include('inc/foot.php'); ?>