<?php include('inc/head.php'); ?>
    <body class="page-developer">
<?php include('inc/header.php'); ?>
    <main id="main">
        <div class="el-section section-intro">
            <div class="el-float-obj num-1"><img src="../public/i/fo-31.png" srcset="../public/i/fo-31@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2"><img src="../public/i/fo-30.png" srcset="../public/i/fo-30@2x.png 2x" alt=""></div>
                <div class="el-float-obj num-3"><img src="../public/i/fo-32.png" srcset="../public/i/fo-32@2x.png 2x" alt=""></div>
                <div class="block-text formated-output">
                    <h1 class="el-h1">vaša investícia <br><span class="hc-text-regular hc-color-primary">naše skúsenosti</span></h1>
                    <div class="el-text-offset hc-text-large">
                        <p>Spoločnosť AIRAVATA HOLDING s.r.o. sa venuje od svojho založenia prostredníctvom svojich dcérskych spoločností investičným projektom v oblasti cestovného ruchu, poskytovania ubytovacích služieb a prevádzkovaniu športových areálov. Jej najvýznamnejšími investíciami je projekt zimného štadióna v Pezinku, ubytovacie zariadenia pri Trnave a Nitre, apartmánový dom v Bratislave, simulátor voľného pádu v Madride a Liptovskom Mikuláši.</p>
                        <p class="hc-align-center"><a href="/" target="_blank"><img src="../public/i/img04.png" srcset="../public/i/img04@2x.png 2x" alt=""></a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-benefits">
            <div class="el-float-obj num-1"><img src="../public/i/fo-35.png" srcset="../public/i/fo-35@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2"><img src="../public/i/fo-33.png" srcset="../public/i/fo-33@2x.png 2x" alt=""></div>
                <div class="el-float-obj num-3"><img src="../public/i/fo-34.png" srcset="../public/i/fo-34@2x.png 2x" alt=""></div>
                <div class="block-text formated-output">
                    <h2 class="block-title el-h3">VYBERTE SI Z NAŠEJ <br><span class="hc-text-regular hc-color-secondary">PONUKY APARTMÁNOV</span></h2>
                    <h3 class="el-h3">3 kľúčové benefity vašej investície</h3>
                    <div class="el-text-offset">
                        <i class="icon-arrow"></i>
                        <div class="el-cols">
                            <div class="col">
                                <h4 class="el-h4">maximálny <br>úžitok</h4>
                                <p>Príjem z prenájmu izieb v časoch, keď ich nebudete využívať.</p>
                            </div>
                            <div class="col formated-output">
                                <h4 class="el-h4">bezstarostný <br>prenájom</h4>
                                <p>Nechajte na nás všetky starosti súvisiace s prenájmom apartmánov.</p>
                            </div>
                            <div class="col formated-output">
                                <h4 class="el-h4">garancia <br>zachovania stavu</h4>
                                <p>Vráťte sa do svojho apartmánu v stave, v akom ste ho opustili.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-1">
            <div class="inner">
                <div class="el-float-obj num-1"><img src="../public/i/fo-36.png" srcset="../public/i/fo-36@2x.png 2x" alt=""></div>
                <div class="block-halves image-left">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2">VÝBORNÁ <br><span class="hc-text-regular">INVESTÍCIA</span></h2>
                        <div class="el-text-offset">
                            <h3 class="el-h4">investícia do nehnuteľností</h3>
                            <p>Využite jedinečnú možnosť investovať do svojej vlastnej nehnuteľnosti, ktorá Vám poskytne na jednej strane zaručene skvelé miesto pre Váš oddych a na strane druhej zároveň poskytne príležitosť benefitovať s jej ďalšieho využívania klientami keď práve apartmány nebudete využívať Vy so svojimi priateľmi alebo rodinou.</p>
                            <p>Zabezpečíme úplnú flexibilitu pri využívaní apartmánu pre vlastné potreby a zároveň ju vo Vami vybraných časoch ponúkneme návštevníkom Demänovskej Doliny.</p>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="../public/i/slide03.jpg" srcset="../public/i/slide03.jpg 2x">
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-2">
            <div class="el-float-obj num-1"><img src="../public/i/fo-37.png" srcset="../public/i/fo-37@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="block-halves image-right">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2">provízia <br><span class="hc-text-regular">pre vás</span></h2>
                        <div class="el-text-offset">
                            <p>Zabezpečte maximálnu úžitkovú hodnotu Vašej nehnuteľnosti, ktorá Vám bude zarábať. Máte
                                možnosť poskytnúť svoj priestor v čase keď ho nevyužívate a profitovať z jej
                                vyťažovania.</p>
                            <p>Výhodou pre Vás je skutočnosť, že obsadenosť a komunikáciu s potenciálnym klientom
                                necháte na nás a my už zabezpečíme maximálne možnú obsadenosť chaletov, ktorú dosiahneme
                                na základe vysokej miery propagácie chaletov a výbornými vzťahmi s hotelovými
                                zariadeniami v okolí.</p>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="../public/i/slide04.jpg" srcset="../public/i/slide04.jpg 2x">
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-3">
            <div class="inner">
                <div class="block-halves image-left">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2">garantované <br><span class="hc-text-regular">služby</span></h2>
                        <div class="el-text-offset">
                            <p>Predaj hotových apartmánov, resp. celých chaletov je založený na predaji konkrétneho apartmánu, resp. celého chaletu. Garantované však zostanú komplexné služby v rámci správy a údržby objektov:</p>
                            <ul>
                                <li>služby údržbára – kontrola stavu objektu</li>
                                <li>upratovacie služby – vonkajšie plochy, po dohode i vnútorné</li>
                                <li>správa inžinierskych sietí</li>
                                <li>požiarna ochrana</li>
                                <li>starostlivosť o zeleň a parkovisko</li>
                            </ul>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="../public/i/slide05.jpg" srcset="../public/i/slide05.jpg 2x">
                    </div>
                </div>
            </div>
        </div>
        <?php include('inc/section-contact.php'); ?>
    </main>
<?php include('inc/footer.php'); ?>
<?php include('inc/foot.php'); ?>