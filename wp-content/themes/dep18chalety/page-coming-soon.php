<?php /* Template Name: Coming soon */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="page-coming-soon">
    <head>
        <!-- META -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116300085-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-116300085-1');
        </script>

        <!-- WP_HEAD -->
        <?php wp_head(); ?>
        <!-- END WP_HEAD -->

        <!-- TODO -->
        <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/public/i/favicon.png" type="image/png">

        <meta name="MobileOptimized" content="width">
        <meta name="HandheldFriendly" content="true">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, target-densityDpi=160dpi">
        <meta http-equiv="cleartype" content="on">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <link rel="stylesheet" href="https://use.typekit.net/unq7exg.css">
        <link href="<?= get_stylesheet_directory_uri() ?>/public/css/main.min.css?<?= @filemtime(get_stylesheet_directory() . '/public/css/main.min.css') ?>" rel="stylesheet" type="text/css">

        <!-- script for loading page cookie -->
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

    </head>
<body>

<body>

<div id="main-wrapper">

    <div class="logo">
        <img src="<?php echo get_template_directory_uri(); ?>/public/i/chalety.png" alt="Chalety Lúčky">
    </div>

    <div class="overview">
        <img src="<?php echo get_template_directory_uri(); ?>/public/i/overview.jpg" alt="Chalety Lúčky">
    </div>

    <hgroup class="headline">
        <h1>APARTMÁNY NA PREDAJ</h1>
        <h2>V lokalite Jasná Lúčky</h2>
    </hgroup>

    <div class="content">

        <p class="dark" >Stránku práve pripravujeme</p>

        <div class="request-quote">
            <p class="form-label">
                Podrobnejšie info a cenník Vám pošleme na vyžiadanie.
            </p>
            <?= do_shortcode('[contact-form-7 id="290" title="Cenník na vyžiadanie"]') ?>
        </div>

    </div>

</div>

<?php get_template_part('templates/partials/footer') ?>