<?php /* Template Name: Domov */ ?>

<?php get_template_part('templates/partials/header') ?>

    <body id="body" class="page-front <?php if(!$_COOKIE['preloader']): ?>landing-visible<?php endif ?>">
<div id="main-wrapper">

    <?php get_template_part('templates/partials/landing') ?>
    <?php get_template_part('templates/partials/header2') ?>
    <main id="main">
        <div class="el-section section-slider parallax-mover" >
            <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-01.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-01@2x.png 2x" style="width: 153px; height: 349px;" alt=""></div>
            <div class="el-float-obj num-3 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-03.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-03@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2 parallax-mover-item" data-parallax-range-min="150" data-parallax-range="-200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-02.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-02@2x.png 2x" style="width: 445px; height: 455px;" alt=""></div>
                <div class="el-float-obj num-4 parallax-mover-item" data-parallax-range-min="150" data-parallax-range="-200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-04.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-04@2x.png 2x" alt=""></div>
                <div class="el-slider">
                    <div class="badge">
                        <div class="inner">
                            <?php $home_main_gal_texts=get_field('home_main_gal_texts')?>
                            <p class="hc-color-black"><?= $home_main_gal_texts['buble']['text_up'] ?></p><p class="hc-color-white"><?= $home_main_gal_texts['buble']['text_down'] ?></p>
                        </div>
                    </div>
                    <div class="items">
                        <?php foreach (get_field('hero_slider') as $slide): ?>
                            <div class="item"><a class="img-bg" style="background-image:url('<?= $slide['image'] ?>');"></a></div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-why parallax-mover">
            <div class="inner">
                <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-05.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-05@2x.png 2x" style="width: 175px; height: 89px;" alt=""></div>
                <div class="el-float-obj num-2 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-06.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-06@2x.png 2x" alt=""></div>
                <div class="block-text">
                    <h2 class="block-title el-h3"><?= $home_main_gal_texts['why']['text_up'] ?><br><span class="hc-text-regular hc-color-secondary"><?= $home_main_gal_texts['why']['text_down'] ?></span></h2>
                    <div class="el-text-offset">
                        <i class="icon-arrow"></i>
                        <div class="el-cols">
                            <div class="col formated-output">
                                <h4 class="el-h4"><?= $home_main_gal_texts['home_investment'][0]['title'] ?><br> <br></h4>
                                <p><?= $home_main_gal_texts['home_investment'][0]['text'] ?></p>
                            </div>
                            <div class="col formated-output">
                                <h4 class="el-h4"><?= $home_main_gal_texts['home_investment'][1]['title'] ?></h4>
                                <p><?= $home_main_gal_texts['home_investment'][1]['text'] ?></p>
                            </div>
                            <div class="col formated-output">
                                <h4 class="el-h4"><?= $home_main_gal_texts['home_investment'][2]['title'] ?></h4>
                                <p><?= $home_main_gal_texts['home_investment'][2]['text'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-about parallax-mover">
            <div class="inner">
                <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="50" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-07.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-07@2x.png 2x" style="width: 217px; height: 159px;" alt=""></div>
                <div class="block-halves block-halves--text-offset image-right">
                    <div class="text-wrap formated-output">
                        <h3 class="el-h2"><?= get_field('home_o_projekte')['title_main'] ?></h3>
                        <div class="el-text-offset">
                            <i class="icon-cottage"></i>
                            <h4 class="el-h4"><?= get_field('home_o_projekte')['title'] ?></h4>
                            <p><?= get_field('home_o_projekte')['text'] ?></p>
                            <p><?= get_field('home_o_projekte')['text_02'] ?></p>
                            <a href="<?= get_field('link_projekt', 'options'); ?>" class="el-btn">zistiť viac</a>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?php echo get_template_directory_uri(); ?>/public/i/img01.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/img01.png 2x" class="upscale_1-5" >
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-gallery parallax-mover">
            <a name="galeria"></a>
            <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-09.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-09@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-2 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-10.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-10@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-3 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-12.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-12@2x.png 2x" style="width: 163px; height: 137px;" alt=""></div>
            <div class="el-float-obj num-4 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-13.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-13@2x.png 2x" style="width: 179px; height: 169px;" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-5 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-08.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-08@2x.png 2x" alt=""></div>
                <div class="el-float-obj num-6 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-11.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-11@2x.png 2x" alt=""></div>
                <div class="tabs-wrap">
                    <div class="block-text">
                        <h2 class="section-title el-h2 hc-color-primary"><?= get_field('gallery_title_main') ?></h2>
                        <ul class="section-tabs">
                            <?php foreach (get_field('gallery') as $index => $category): ?>
                                <li class="tab"><a href="#gallery-<?= $index + 1 ?>" class="noscroll" ><?= $category['name'] ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <?php foreach (get_field('gallery') as $index => $category): ?>
                        <div class="tab-panel" id="gallery-<?= $index + 1 ?>">
                            <div class="el-gallery">
                                <div class="items">
                                    <?php foreach ($category['images'] as $image): ?>
                                        <div class="item"><a href="<?= $image['image'] ?>" class="img-bg" style="background-image:url('<?= $image['image'] ?>');"></a></div>
                                    <?php endforeach ?>
                                </div>
                                <div class="thumbnails">
                                    <?php foreach ($category['images'] as $image): ?>
                                        <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('<?= $image['image'] ?>');"></span></div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        <div class="el-section section-locality parallax-mover">
            <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-800" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-14.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-14@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-2 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-1600" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-15.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-15@2x.png 2x" alt=""></div>
            <div class="inner" >
                <div class="el-float-obj num-3 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-1600" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-16.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-16@2x.png 2x" style="width: 254px; height: 280px;" alt=""></div>
                <div class="el-float-obj num-4 parallax-mover-item-x" data-parallax-range-min="150" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-17.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-17@2x.png 2x" style="width: 574px; height: 217px;" alt=""></div>
                <div class="block-halves image-right">
                    <div class="text-wrap block-text formated-output">
                        <h3 class="el-h2 hc-color-primary section-title"><?= get_field('home_lokalita')['title_main'] ?></h3>
                        <div class="el-text-offset">
                            <i class="icon-tree2"></i>
                            <h4 class="el-h4"><?= get_field('home_lokalita')['title'] ?></h4>
                            <p><?= get_field('home_lokalita')['text'] ?></p>
                            <a href="<?= get_field('link_lokalita', 'options'); ?>" class="el-btn">viac o lokalite</a>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <div class="map-wrap">
                            <?php $home_map = get_field('home_lokalita')['map'] ?>
                            <div class="map-embed" data-lat="<?= $home_map['lat'] ?>" data-lng="<?= $home_map['lng'] ?>" data-zoom="<?= $home_map['zoom'] ?>" ></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-visualization parallax-mover">
            <div class="inner">
                <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-18.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-18@2x.png 2x" alt=""></div>
                <?php // get_template_part('templates/partials/block-visualization') ?>

                <div class="section-3d-nav">
                    <?php /* Find a page based on the page-apartman template */ ?>
                    <?php
                    $apartmentPage = get_pages(array(
                        'meta_key' => '_wp_page_template',
                        'meta_value' => 'page-apartman.php'
                    ));
                    ?>
                    <div class="full-nav">
                        <div class="block-text">
                            <h2 class="section-title el-h2 hc-color-primary">3d <span class="hc-text-light">vizualizácia</span></h2>
                        </div>
                        <div class="block-visualization">
                            <?php echo do_shortcode( '[drawattention]' ); ?>
                            <?php foreach (get_field('homes', 'options') as $index => $home): ?>
                                <?php /* Regroup apartments array by floors */ ?>
                                <?php $floors = []; ?>
                                <?php foreach ($home['apartments'] as $apartment): ?>
                                    <?php
                                    if(!isset($floors[$apartment['floor']])){
                                        $floors[$apartment['floor']] = [];
                                    }
                                    $floors[$apartment['floor']][] = $apartment;
                                    ?>
                                <?php endforeach ?>
                                <?php /* Create flyers */ ?>
                                <?php foreach ($floors as $floorId => $floorApartments): ?>
                                    <?php
                                        // check if all apartments are sold
                                        $floorSold = true;
                                        foreach ($floorApartments as $apartment){
                                            if ($apartment['ap_state']=='free'){
                                                $floorSold = false;
                                            }
                                        }
                                    ?>
                                    <div class="desc-wrap <?php if($floorSold): ?>desc-wrap--sold<?php endif ?>" data-id="chalet_<?= $home['id'] ?>_<?= $floorId ?>" >
                                        <h4 class="title">Chalet č.<?= $home['id'] ?></h4>
                                        <?php foreach ($floorApartments as $apartment): ?>
                                            <div class="apartment-label <?php if($apartment['ap_state']=='free'): ?>apartment-label--free<?php else:?>apartment-label--sold<?php endif ?>">Apartmán č. <?= $apartment['number'] ?></div>
                                        <?php endforeach ?>
                                        <a href="<?= get_permalink($apartmentPage[0]->ID) ?>#podorys_<?= $home['id'] ?>_<?= $floorId ?>" class="btn"><?php if($floorSold): ?>Predané<?php else: ?>Zobraziť<?php endif ?></a>
                                    </div>
                                <?php endforeach ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <?php if(count($apartmentPage)): ?>
                        <div class="select-nav" >
                            <div class="block-text">
                                <h2 class="section-title el-h2 hc-color-primary">Výber <span class="hc-text-light">podlažia</span></h2>
                            </div>
                            <div class="select-wrap">
                                <select class="select-redirect" data-href="<?= get_permalink($apartmentPage[0]->ID) ?>" >
                                    <?php foreach (get_field('homes', 'options') as $index => $home): ?>
                                        <?php /* Regroup apartments array by floors */ ?>
                                        <?php $floors = []; ?>
                                        <?php foreach ($home['apartments'] as $apartment): ?>
                                            <?php
                                            if(!isset($floors[$apartment['floor']])){
                                                $floors[$apartment['floor']] = [];
                                            }
                                            $floors[$apartment['floor']][] = $apartment;
                                            ?>
                                        <?php endforeach ?>
                                        <?php /* Create options */ ?>
                                        <?php foreach ($floors as $floorId => $floorApartments): ?>
                                            <option value="<?= $home['id'] ?>_<?= $floorId ?>" >
                                                <?= $home['name'] ?>: <?= $floorApartments[0]['floor_name'] ?>
                                            </option>
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    <?php endif ?>
                </div>

            </div>
        </div>
        <div class="el-section section-developer parallax-mover">
            <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="600" data-parallax-range="-1200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-19.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-19@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-800" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-20.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-20@2x.png 2x" style="width: 256px; height: 139px;" alt=""></div>
                <div class="block-text">
                    <?php $home_dev = get_field('home_developer')?>
                    <h2 class="section-title el-h2 hc-color-primary"><?= $home_dev['title_main'] ?></h2>
                </div>
                <div class="block-halves image-right">
                    <div class="text-wrap block-text formated-output">
                        <div class="el-text-offset">
                            <i class="icon-tree1"></i>
                            <h4 class="el-h4"><?= $home_dev['title']['title_up'] ?><br><span class="hc-color-secondary"><?= $home_dev['title']['title_down'] ?></span></h4>
                            <p><?= $home_dev['text'] ?></p>
                            <a href="<?= get_field('link_developer', 'options'); ?>?>" class="el-btn">viac o developerovi</a>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <?php if(get_field('homepage_url', 'options')): ?>
                            <a href="<?= get_field('homepage_url', 'options') ?>" target="_blank">
                        <?php endif ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/public/i/logo-1.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/logo-1@2x.png 2x" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <?php get_template_part('templates/partials/section-contact') ?>
    </main>
    <?php get_template_part('templates/partials/footer2') ?>
</div>
<?php get_template_part('templates/partials/footer') ?>