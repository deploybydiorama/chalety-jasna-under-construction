<?php /* Template Name: Lokalita */ ?>

<?php get_template_part('templates/partials/header') ?>
<body class="page-locality">
<div id="main-wrapper">
    <?php get_template_part('templates/partials/header2') ?>
    <main id="main">
        <div class="el-section section-intro parallax-mover">
            <div class="inner">
                <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="150" data-parallax-range="-300" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-25.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-25@2x.png 2x" style="width: 237px; height: 119px;" alt=""></div>
                <div class="block-text formated-output">
                    <h1 class="el-h1">lokalita</h1>
                    <div class="el-text-offset hc-text-large">
                        <?= get_field('text') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-map">
            <div class="inner-wrap">
                <div class="map-wrap">
                    <?php $map = get_field('map') ?>
                    <div class="map-embed" data-lat="<?= $map['lat'] ?>" data-lng="<?= $map['lng'] ?>" data-zoom="<?= $map['zoom'] ?>" ></div>
<!--                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d41757.343416225674!2d19.347589222281133!3d49.14677679944373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4715a5b288071c3b%3A0x400f7d1c6970f30!2zMDM0IDgyIEzDusSNa3k!5e0!3m2!1sen!2ssk!4v1519725191228" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                </div>
            </div>
        </div>
        <div class="el-section section-benefits parallax-mover" >
            <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="800" data-parallax-range="-1600" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-27.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-27@2x.png 2x" style="width: 268px; height: 602px;" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2  parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-26.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-26@2x.png 2x" alt=""></div>
                <div class="block-text formated-output">
                    <h2 class="el-h3">dostupnosť</h2>
                    <div class="el-text-offset">
                        <i class="icon-arrow"></i>
                        <div class="el-cols cols-2">
                            <div class="col">
                                <ul>
                                    <?php foreach(get_field('availability')['col_1'] as $item): ?>
                                    <li><?= $item['text'] ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="col">
                                <ul>
                                    <?php foreach(get_field('availability')['col_2'] as $item): ?>
                                        <li><?= $item['text'] ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-1 parallax-mover">
            <div class="inner">
                <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="800" data-parallax-range="-1600" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-28.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-28@2x.png 2x" alt=""></div>
                <div class="block-halves image-left">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2"><?= get_field('highlights')[0]['title'] ?></h2>
                        <div class="el-text-offset">
                            <?= get_field('highlights')[0]['text'] ?>
                            <?php $badge = get_field('highlights')[0]['badge']; ?>
                            <?php if ($badge): ?>
                                <div class="el-badge-text">
                                    <div class="el-badge-text__badge el-badge-text__badge--sm">
                                        <img src="<?= $badge[0]['image']; ?>" alt="">
                                    </div>
                                    <div class="el-badge-text__text">
                                        <?= $badge[0]['text']; ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?= get_field('highlights')[0]['image'] ?>" class="upscale_1-25_x-15_y0" >
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-2">
            <div class="inner">
                <div class="block-halves image-right">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2"><?= get_field('highlights')[1]['title'] ?></h2>
                        <div class="el-text-offset">
                            <?= get_field('highlights')[1]['text'] ?>
                            <?php $badge = get_field('highlights')[1]['badge']; ?>
                            <?php if ($badge): ?>
                                <div class="el-badge-text">
                                    <div class="el-badge-text__badge el-badge-text__badge--sm">
                                        <img src="<?= $badge[0]['image']; ?>" alt="">
                                    </div>
                                    <div class="el-badge-text__text">
                                        <?= $badge[0]['text']; ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?= get_field('highlights')[1]['image'] ?>" class="upscale_1-25_x15_y0" >
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-3 parallax-mover" >
            <div class="inner">
                <div class="el-float-obj num-1  parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-29.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-29@2x.png 2x" alt="" ></div>
                <div class="block-halves image-left">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2"><?= get_field('highlights')[2]['title'] ?></h2>
                        <div class="el-text-offset">
                            <?= get_field('highlights')[2]['text'] ?>
                            <?php $badge = get_field('highlights')[2]['badge']; ?>
                            <?php if ($badge): ?>
                                <div class="el-badge-text">
                                    <div class="el-badge-text__badge el-badge-text__badge--sm">
                                        <img src="<?= $badge[0]['image']; ?>" alt="">
                                    </div>
                                    <div class="el-badge-text__text">
                                        <?= $badge[0]['text']; ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?= get_field('highlights')[2]['image'] ?>" class="upscale_1-25_x-15_y0" >
                    </div>
                </div>
            </div>
        </div>
        <?php get_template_part('templates/partials/section-contact') ?>
    </main>
<?php get_template_part('templates/partials/footer2') ?>
</div>
<?php get_template_part('templates/partials/footer') ?>