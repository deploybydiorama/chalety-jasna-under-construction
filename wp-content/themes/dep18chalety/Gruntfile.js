module.exports = function (grunt) {

    // autoloads npm tasks
    require('load-grunt-tasks')(grunt);


    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            images: {
                files: ['assets/i/**/*.{svg,gif,png,jpg,jpeg}'],
                tasks: ['images']
            },
            js: {
                files: ['assets/js/**/*.js'],
                tasks: ['js']
            },
            css: {
                files: ['assets/scss/**/*.scss'],
                tasks: ['css']
            },
            livereload: {
                files: ['public/js/*.js', 'public/css/*.css'],
                options: {livereload: true}
            }
        },
        // ****************** JS ******************
        concat: {
            options: {
                sourceMap: true
            },
            main: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/underscore/underscore.js',
                    'bower_components/slick-carousel/slick/slick.js',
                    'assets/js/plugins/jquery.magnific-popup.min.js',
                    'assets/js/plugins/jquery.easytabs.min.js',
                    'assets/js/main/**/*.js'
                ],
                dest: 'temp/grunt/js/main.js'
            }
        },
        babel: {
            options: {
                sourceMap: true,
                plugins: ['transform-react-jsx'],
                presets: ['es2015', 'react']
            },
            main: {
                src: [
                    'temp/grunt/js/main.js'
                ],
                dest: 'temp/grunt/js/main.es5.js'
            }
        },
        uglify: {
            options: {
                sourceMap: true
            },
            main: {
                src: [
                    'temp/grunt/js/main.js'
                ],
                dest: 'public/js/main.min.js'
            }
        },
        // ****************** CSS ******************
        sass: {
            options: {
                sourceMap: true,
                outputStyle: 'compressed'
            },
            main: {
                expand: true,
                cwd: 'assets/scss/main/',
                src: '**/*.scss',
                dest: 'temp/grunt/css/compiled/main',
                ext: '.css'
            }
        },
        autoprefixer: {
            options: {
                map: true
            },
            main: {
                expand: true,
                cwd: 'temp/grunt/css/compiled/main',
                src: '**/*.css',
                dest: 'temp/grunt/css/autoprefixed/main'
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 0,
                sourceMap: true,
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            main: {
                files: {
                    'public/css/main.min.css': [
                        'temp/grunt/css/autoprefixed/main/**/*.css'
                    ]
                }
            }
        },
        // ****************** IMAGES ******************,
        copy: {
            images: {
                files: [{
                    expand: true,
                    cwd: 'assets/i/',
                    src: ["**/*.{svg,gif}"],
                    dest: "public/i/"
                }]
            }
        },
        tinypng: {
            options: {
                apiKey: '23V-nuBTBtyMPaRsK6Fs_cBjwc7bfoRe',
                checkSigs: true,
                sigFile: 'assets/i/.tinypng.sigfile',
                sigFileSpace: 4,
                summarize: false,
                showProgress: false
            },
            images: {
                files: [{
                    expand: true,
                    cwd: 'assets/i/',
                    src: ["**/*.{png,jpg,jpeg}"],
                    dest: "public/i/"
                }]
            }
        }
    });

    // register tasks
    grunt.registerTask('images', ['copy', 'tinypng']);
    grunt.registerTask('css', ['sass', 'autoprefixer', 'cssmin']);
    grunt.registerTask('js', ['concat', 'uglify']);

    grunt.registerTask('default', ['images', 'css', 'js']);
    //grunt.registerTask('default', ['images', 'css']);

};