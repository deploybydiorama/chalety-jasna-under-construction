<?php /* Template Name: Wellness */ ?>

<?php get_template_part('templates/partials/header') ?>
<body id="body" class="page-wellness">
<div id="main-wrapper">
    <?php get_template_part('templates/partials/header2') ?>
    <main id="main">

        <div class="el-section section-intro parallax-mover">
            <div class="inner">
                <div class="block-text formated-output">
                    <h1 class="el-h1"><?= get_field('title_main') ?></h1>
                    <div class="el-text-offset hc-text-large">
                        <?= get_field('main_text') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="el-section section-gallery parallax-mover">
            <a name="galeria"></a>
            <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-09.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-09@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-2 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-10.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-10@2x.png 2x" alt=""></div>
            <div class="el-float-obj num-3 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-12.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-12@2x.png 2x" style="width: 163px; height: 137px;" alt=""></div>
            <div class="el-float-obj num-4 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-13.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-13@2x.png 2x" style="width: 179px; height: 169px;" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-5 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-08.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-08@2x.png 2x" alt=""></div>
                <div class="el-float-obj num-6 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-11.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-11@2x.png 2x" alt=""></div>
                <div class="tabs-wrap">
                        <div class="tab-panel active" id="gallery-1">
                            <div class="el-gallery">
                                <div class="items">
                                    <?php foreach (get_field('hero_slider') as $slide): ?>
                                        <div class="item"><a href="<?= $slide['image'] ?>" class="img-bg" style="background-image:url('<?= $slide['image'] ?>');"></a></div>
                                    <?php endforeach ?>
                                </div>
                                <div class="thumbnails">
                                    <?php foreach (get_field('hero_slider') as $slide): ?>
                                        <div class="thumbnail"><span class="in"><span class="img-bg" style="background-image:url('<?= $slide['image'] ?>');"></span></div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
        <?php get_template_part('templates/partials/section-contact') ?>
    </main>
    <?php get_template_part('templates/partials/footer2') ?>
</div>
<?php get_template_part('templates/partials/footer') ?>