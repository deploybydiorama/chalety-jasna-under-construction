<?php /* Template Name: Developer */ ?>

<?php get_template_part('templates/partials/header') ?>
<body class="page-developer">
<div id="main-wrapper">
    <?php get_template_part('templates/partials/header2') ?>
        <main id="main">
            <div class="el-section section-intro parallax-mover">
                <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-31.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-31@2x.png 2x" style="width: 216px; height: 204px;" alt=""></div>
                <div class="inner">
                    <div class="el-float-obj num-2 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-30.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-30@2x.png 2x" style="width: 220px; height: 165px;" alt=""></div>
                    <div class="el-float-obj num-3 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-32.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-32@2x.png 2x" alt=""></div>
                    <div class="block-text formated-output">
                        <h1 class="el-h1">vaša investícia <br><span class="hc-text-regular hc-color-primary">naše skúsenosti</span></h1>
                        <div class="el-text-offset hc-text-large">
                            <p><?= get_field('developer_main_text') ?></p>
                            <p class="hc-align-center intro-logo">

                            <?php if(get_field('homepage_url','options')): ?>
                            <a href="<?= get_field('homepage_url', 'options') ?>" target="_blank">
                            <?php endif ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/public/i/img05.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/img05@2x.png 2x" alt=""></a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="el-section section-benefits parallax-mover" >
                <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="100" data-parallax-range="-200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-35.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-35@2x.png 2x" style="width: 608px; height: 316px;" alt=""></div>
                <div class="inner">
                    <div class="el-float-obj num-2 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-33.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-33@2x.png 2x" style="width: 110px; height: 138px;" alt=""></div>
                    <div class="el-float-obj num-3 parallax-mover-item-x" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-34.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-34@2x.png 2x" alt=""></div>
                    <div class="block-text formated-output">
                        <h2 class="block-title el-h3">VYBERTE SI Z NAŠEJ <br><span class="hc-text-regular hc-color-secondary">PONUKY APARTMÁNOV</span></h2>
                        <h3 class="el-h3">3 kľúčové benefity vašej investície</h3>
                        <div class="el-text-offset">
                            <i class="icon-arrow"></i>
                            <div class="el-cols">
                                <div class="col">
                                    <h4 class="el-h4"><?= get_field('benefity')[0]['title'] ?></h4>
                                    <p><?= get_field('benefity')[0]['text'] ?></p>
                                </div>
                                <div class="col formated-output">
                                    <h4 class="el-h4"><?= get_field('benefity')[1]['title'] ?></h4>
                                    <p><?= get_field('benefity')[1]['text'] ?></p>
                                </div>
                                <div class="col formated-output">
                                    <h4 class="el-h4"><?= get_field('benefity')[2]['title'] ?></h4>
                                    <p><?= get_field('benefity')[2]['text'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="el-section section-item-1 parallax-mover" >
                <div class="inner">
                    <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-36.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-36@2x.png 2x" alt=""></div>
                    <div class="block-halves image-left">
                        <div class="text-wrap formated-output">
                            <h2 class="el-h2--regular"><?= get_field('developer_highlights')[0]['title'] ?></h2>
                            <div class="el-text-offset">
                                <h3 class="el-h4"><?= get_field('developer_highlights')[0]['subtitle'] ?></h3>
                                <?= get_field('developer_highlights')[0]['text'] ?>
                            </div>
                        </div>
                        <div class="image-wrap">
                            <img src="<?= get_field('developer_highlights')[0]['image'] ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="el-section section-item-2 parallax-mover" >
                <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="100" data-parallax-range="-200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-37.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-37@2x.png 2x" alt=""></div>
                <div class="inner">
                    <div class="block-halves image-right">
                        <div class="text-wrap formated-output">
                            <h2 class="el-h2--regular"><?= get_field('developer_highlights')[1]['title'] ?></h2>
                            <div class="el-text-offset">
                                <h3 class="el-h4"><?= get_field('developer_highlights')[1]['subtitle'] ?></h3>
                                <?= get_field('developer_highlights')[1]['text'] ?>
                            </div>
                        </div>
                        <div class="image-wrap">
                            <img src="<?= get_field('developer_highlights')[1]['image'] ?>" >
                        </div>
                    </div>
                </div>
            </div>
            <div class="el-section section-item-3">
                <div class="inner">
                    <div class="block-halves image-left">
                        <div class="text-wrap formated-output">
                            <h2 class="el-h2--regular"><?= get_field('developer_highlights')[2]['title'] ?></h2>
                            <div class="el-text-offset">
                                <?= get_field('developer_highlights')[2]['text'] ?>
                            </div>
                        </div>
                        <div class="image-wrap">
                            <img src="<?= get_field('developer_highlights')[2]['image'] ?>" >
                        </div>
                    </div>
                </div>
            </div>
            <?php get_template_part('templates/partials/section-contact') ?>
        </main>
    <?php get_template_part('templates/partials/footer2') ?>
</div>
<?php get_template_part('templates/partials/footer') ?>