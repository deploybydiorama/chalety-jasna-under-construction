var MousePositionMover = {
    ui: {
        container: '.mouse-position-mover',
        item: '.mouse-position-mover-item',
    },
    mouse: {x: 0, y: 0},
    $container: null,
    ready: function () {

        this.$container = $(this.ui.container);

        this.bind();

        // init settings
        this.$container.css('perspective', this.$container.data('position-distance') + 'px');
        $(this.ui.item).each(function () {
            $(this).css('transform', 'translateZ(' + $(this).data('position-z') + 'px)');
        });

        // run
        this.monitor(true);

    },
    bind: function(){

        var self = this;

        // bind
        $(window).mousemove(function(e){
            self.update(e);
        });

    },
    monitor: function(first){

        var self = this;

        this.updatePerspective(first);
        requestAnimationFrame(self.monitor.bind(self, false));

    },
    update: function(e){

        this.mouse.x = e.pageX;
        this.mouse.y = e.pageY;

    },
    updatePerspective: function(first){

        if (first){
            this.$container.addClass('mouse-position-mover-initialized');
        }
        this.$container.css('perspective-origin', this.mouse.x + 'px ' + this.mouse.y + 'px');

    }
};

$(function(){
    MousePositionMover.ready();
});