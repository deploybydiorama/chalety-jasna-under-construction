window.screenXL = 1400;
window.screenLG = 1100;
window.screenMD = 992;
window.screenSM = 767;
window.screenXS = 480;
window.screenUS = 380;
(function ($) {
    $(document).ready(function(){
    //-------------------------------------------
    //true window dimensions
    function viewport() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }
    //-------------------------------------------
    //get transform
    function fnGetTransformVal(el) {
        var $el = el,
            $matrix = $el.css('transform'),
            $translateVal = $matrix.match(/-?[\d\.]+/g),
            $translate = 0;
        if($translateVal!=null){$translate=parseInt($translateVal[5]);}
        return ($translate);
    }
    //-------------------------------------------
    //toggle body class
    function fnToggle(cl,action) {
        var $activeClass = cl,
            $elClass = $('body');
        if(action=='close'){
            $elClass.removeClass($activeClass);
        }else{
            if($elClass.hasClass($activeClass)==true){
                $elClass.removeClass($activeClass);
            }else{
                $elClass.addClass($activeClass);
            }
        }
    }
    //-------------------------------------------
    //scroll functionality
    // $('a[href*="#"]:not([href="#"])').click(function(e) {
    //     if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname){
    //         var target = $(this.hash);
    //         target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    //         if (target.length) {
    //             $('html,body').animate({
    //                 scrollTop: target.offset().top
    //             }, 500);
    //             e.preventDefault();
    //         }
    //     }
    // });
    //-------------------------------------------
    //el slider
    $('.el-slider .items').slick({
        arrows: true,
        autoplaySpeed: 1000,
        dots: false,
        draggable: true,
        infinite: true,
        prevArrow: '<a class="slick-dir-nav prev"></a>',
        nextArrow: '<a class="slick-dir-nav next"></a>',
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: true
    });
    //-------------------------------------------
    //nav toggle
    function fnToggleNav(action) {
        fnToggle('nav-active',action);
    }
    $(document).on('click','#header .nav-toggle',function(e){
        e.preventDefault();
        fnToggleNav();
    });
    //-------------------------------------------
    //galllery
    function fnGallery(objId,action) {
        var $options1 = {
            arrows: true,
            asNavFor: '#'+objId+' .el-gallery .thumbnails',
            dots: false,
            draggable: true,
            infinite: true,
            prevArrow: '<a class="slick-dir-nav prev"></a>',
            nextArrow: '<a class="slick-dir-nav next"></a>',
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: true
        },
        $options2 = {
            arrows: true,
            asNavFor: '#'+objId+' .el-gallery .items',
            centerMode: true,
            dots: false,
            draggable: true,
            focusOnSelect: true,
            infinite: true,
            prevArrow: '<a class="slick-dir-nav prev"></a>',
            nextArrow: '<a class="slick-dir-nav next"></a>',
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            swipe: true,
            variableWidth: true
        };
        if(action=='create'){
            $('#'+objId+' .el-gallery .items').slick($options1);
            $('#'+objId+' .el-gallery .thumbnails').slick($options2);
        }
        if(action=='destroy'){
            $('#'+objId+' .el-gallery .items').slick('unslick');
            $('#'+objId+' .el-gallery .thumbnails').slick('unslick');
        }
    }
    var $activeGalleryTab = $('.page-front .section-gallery .tab-panel').attr('id');
    fnGallery($activeGalleryTab,'create');

    $('.page-front .section-gallery .tabs-wrap').easytabs({
        animate: true,
        animationSpeed: 250,
        collapsedByDefault: false,
        tabs: ".section-tabs > li",
        updateHash: false
    });
    $('.page-front .section-gallery .tabs-wrap')
        .bind('easytabs:midTransition', function(event, $clicked, $targetPanel, settings) {
            var $actTabId = $(this).find('.tab-panel.active').attr('id');
            fnGallery($actTabId,'create');
            fnGallery($activeGalleryTab,'destroy');
            $activeGalleryTab = $actTabId;
    });
    $('.el-gallery').magnificPopup({
        delegate: '.item a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        image: {
            verticalFit: true,
            titleSrc: function(item) {
                return item.el.find('img').attr('alt');
            }
        },
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1],
            tCounter: '%curr%/%total%'
        }
    });


    //gallery wellness

        var $activeGalleryTab1 = $('.page-wellness .section-gallery .tab-panel').attr('id');
        fnGallery($activeGalleryTab1,'create');

/*        $('.page-wellness .section-gallery .tabs-wrap').easytabs({
            animate: true,
            animationSpeed: 250,
            collapsedByDefault: false,
            tabs: ".section-tabs > li",
            updateHash: false
        });*/
        $('.page-wellness .section-gallery .tabs-wrap')
            .bind('easytabs:midTransition', function(event, $clicked, $targetPanel, settings) {
                var $actTabId = $(this).find('.tab-panel.active').attr('id');
                fnGallery($actTabId,'create');
                fnGallery($activeGalleryTab1,'destroy');
                $activeGalleryTab1 = $actTabId;
            });
        $('.el-gallery').magnificPopup({
            delegate: '.item a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            image: {
                verticalFit: true,
                titleSrc: function(item) {
                    return item.el.find('img').attr('alt');
                }
            },
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1],
                tCounter: '%curr%/%total%'
            }
        });





    //-------------------------------------------
    //landing
    function fnLanding(timeout){

        setTimeout(function () {
            $('body').removeClass('landing-visible');
        },timeout);

        // cookies
        // https://github.com/js-cookie/js-cookie
        // time in minutes

        var expireTime = new Date(new Date().getTime() + 10 * 60 * 1000);

        // when expite time should be in days, put expite: <number>
        Cookies.set('preloader', true, {path: '/', expire: expireTime });

    }
    // Preload
    var picsLoaded = 0;
    var picsList = [];
    $('#landing img').each(function(){
        if (this.currentSrc){
            picsList.push(this.currentSrc);
        }else{
            picsList.push(this.src);
        }
    });
    _.each(picsList, function (pic) {
        var img = new Image();
        img.src = pic;
        $(img).on('load', function (e) {
            picsLoaded++;
            if (picsList.length <= picsLoaded){
                $('body').addClass('preloader-complete');
                fnLanding(3000);
            }
        });
    });
    //-------------------------------------------
    //trigers
    $(window).scroll(function(){
    });
    $(window).resize(function(){
    });
    $(document).keyup(function(e) {
        if (e.keyCode === 27){
            fnToggleNav('close');
        }
    });
    //-------------------------------------------
    // draw attentiion
    var hideTimer = null;
    var $areas = $('.block-visualization .hotspots-map area');
    var $flyers = $('.block-visualization .desc-wrap');
    var $select = $('.section-3d-nav .select-nav select');


    //desktop
    $areas.mouseenter(function(e){
        showVisualisationFlyer($(this).attr('alt'), this, e);
        showPlan($(this).attr('alt'), e);
    });
    $areas.mouseleave(function(e){
        timeoutHideVisualisationFlyer($(this).attr('alt'), this, e);
    });

    //mobile

    $areas.on('touchstart', function(e){
        showVisualisationFlyer($(this).attr('alt'), this, e);
        showPlan($(this).attr('alt'), e);
    });

    $flyers.mouseenter(function(e){
        clearTimeout(hideTimer);
    });
    $flyers.mouseleave(function(e){
        timeoutHideVisualisationFlyer($(this).data('id'), this, e);
    });



    $select.change(function(){
        if ($(this).is('.select-redirect')){
            location.href = $(this).data('href') + '#podorys_' + $(this).val();
        }else{
            showPlan($(this).val());
        }
    });
    // parse the onload hash value and show a floor, if requested
    var floorMatch;
    if (floorMatch = /#podorys_([\d]+)_([\d]+)/.exec(location.hash)){
        showPlan('chalet_' + floorMatch[1] + '_' + floorMatch[2]);
        setTimeout(function(){
            Tools.Scroller.scrollTo('podorys');
        }, 500);
    }
    function showPlan(id, event){
        // retrieve elements
        var $plans = $('.block-plan .plan-wrap');
        var $current = $plans.filter('[data-id="' + id + '"]');
        // do the plan exist?
        if (!$current.length){
            return;
        }
        // hide all flyers
        $plans.removeClass('active');
        // show current flyer
        $current.addClass('active');
    }
    function showVisualisationFlyer(id, area, event){
        // hide all flyers
        hideVisualisationFlyers();
        // show current flyer
        var $flyer = $('.block-visualization .desc-wrap').filter('[data-id="' + id + '"]');
        $flyer.addClass('active');
        var coords = $(area).attr('coords').split(',');
        var x = _.filter(_.map(coords, function(v){ return parseInt(v); }), function(v, i){ return i % 2 == 0; });
        var y = _.filter(_.map(coords, function(v){ return parseInt(v); }), function(v, i){ return i % 2 == 1; });
        $flyer.css({
            top: Math.round((_.min(y) + _.max(y)) / 2) + 'px',
            left: (_.max(x) - 20) + 'px'
        });
        // setTimeout(function () {
        //     console.log($flyer);
        //     console.log($flyer.offsetParent());
        //     $flyer.css({
        //         top: event.pageY - $flyer.offsetParent().offset().top + 'px',
        //         left: event.pageX - $flyer.offsetParent().offset().left + 'px'
        //     });
        // }, 0);
    }
    function timeoutHideVisualisationFlyer(id, area, event){
        hideTimer = setTimeout(function(){
            hideVisualisationFlyer(id, area, event);
        }, 1000);
    }
    function hideVisualisationFlyer(id, area, event){
        if (hideTimer){
            clearTimeout(hideTimer);
        }
        $('.block-visualization .desc-wrap').filter('[data-id="' + id + '"]').removeClass('active');
    }
    function hideVisualisationFlyers(){
        if (hideTimer){
            clearTimeout(hideTimer);
        }
        $('.block-visualization .desc-wrap').removeClass('active');
    }
    //-------------------------------------------
    });
})(jQuery)