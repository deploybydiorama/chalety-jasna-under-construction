var ParallaxMover = {
    ui: {
        container: '.parallax-mover',
        child: '.parallax-mover-item',
    },
    data: {
        scroll: null,
        viewport: null,
        doc: null,
        containers: []
    },
    ready: function () {

        var self = this;

        setTimeout(function(){
            self.bind();
        }, 500);

    },
    bind: function(){

        this.init();
        this.monitor(true);

    },
    init: function(){

        var self = this;

        // bind
        $(window).resize(function(){
            self.updateContainers();
        });
        $(window).resize();
        setInterval(function(){
            self.data.scroll = null;
            self.updateContainers();
            self.update(true);
        }, 500);

    },
    monitor: function(first){

        var self = this;

        this.update(first);
        requestAnimationFrame(self.monitor.bind(self, false));

    },
    updateContainers: function () {

        var self = this;

        $(this.ui.container).each(function(index){

            // basic data
            var data = {
                children: []
            };
            // container
            var $container = $(this);
            data.$ = $container;
            data.top = $container.offset().top;
            data.height = $container.offset().top + $container.height();

            // children
            $container.find(self.ui.child).each(function () {
                var $child = $(this);
                data.children.push({
                    $: $child,
                    exists: $child.length ? true : false,
                    rotate: $child.attr('data-parallax-rotate') ? parseInt($child.attr('data-parallax-rotate')) : 0,
                    range: parseInt($child.attr('data-parallax-range')),
                    rangeMin: parseInt($child.attr('data-parallax-range-min')),
                    rangeX: $child.attr('data-parallax-range-x') ? parseInt($child.attr('data-parallax-range-x')) : 0,
                    rangeXMin: $child.attr('data-parallax-range-x-min') ? parseInt($child.attr('data-parallax-range-x-min')) : 0
                });
            });
            // save
            self.data.containers[index] = data;
        });

        // main dimensions
        this.data.viewport = $(window).height();
        this.data.doc = $(document).height();

    },
    update: function(first){

        var self = this;

        // current scroll state
        var scroll = $(window).scrollTop();
        if (this.data.scroll === scroll){
            return;
        }else{
            this.data.scroll = scroll
        }

        // retrieve dimensions
        var viewport = this.data.viewport;
        var doc = this.data.doc;

        // update all containers
        _.each(this.data.containers, function(container){
            self.updateOne(container, viewport, scroll, doc);
            if (first){
                _.each(container.children, function(child){
                    child.$.addClass('parallax-mover-initialized');
                });
            }
        });

    },
    updateOne: function(container, viewport, scroll, doc){

        var self = this;

        var top = container.top;
        var height = container.height;
        var bottom = top + height + viewport;
        // max scroll
        if (doc < bottom){
            bottom = doc;
        }

        var current;
        if (scroll + viewport < top){
            current = 0;
        }else if (scroll + viewport > bottom){
            current = 1;
        }else{
            current = (scroll + viewport - top) / (bottom - top);
        }

        _.each(container.children, function(child){
            self.translate(child, current)
        });

    },
    translate: function(child, ratio){

        var $el = child.$;
        var min = child.rangeMin;
        var range = child.range;
        var translate = ratio * range + min;
        var rotate = ratio * child.rotate;
        var minX = child.rangeXMin;
        var translateX = ratio * child.rangeX + minX;

        $el.css('transform', 'translate3d(' + Math.round(translateX) + 'px, ' + Math.round(translate) + 'px, 0) rotate('  + Math.round(rotate) + 'deg)');

    }
}

$(function(){
    ParallaxMover.ready();
});