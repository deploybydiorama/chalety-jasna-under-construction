<?php /* Template Name: Projekt */ ?>

<?php get_template_part('templates/partials/header') ?>
<body class="page-project">
<div id="main-wrapper">
    <?php get_template_part('templates/partials/header2') ?>
    <main id="main">
        <div class="el-section section-intro parallax-mover">
            <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="200" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-23.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-23@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2 parallax-mover-item-x" data-parallax-range-min="400" data-parallax-range="-800"><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-20.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-20@2x.png 2x" style="width: 256px; height: 139px;" alt=""></div>
                <div class="block-text formated-output">
                    <h1 class="el-h1"><?= get_field('title_main') ?></h1>
                    <div class="el-text-offset hc-text-large">
                        <?= get_field('main_text') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-slider parallax-mover">
            <div class="inner">
                <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="150" data-parallax-range="-400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-24.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-24@2x.png 2x" style="width: 547px; height: 191px;" alt=""></div>
                <div class="el-slider">
                    <div class="items">
                        <?php foreach (get_field('hero_slider') as $slide): ?>
                            <div class="item"><a class="img-bg" style="background-image:url('<?= $slide['image'] ?>');"></a></div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-benefits parallax-mover">
            <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="1200" data-parallax-range="-2400" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-19.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-19@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="block-text">
                    <h2 class="block-title el-h3"><?= get_field('project_benefits')['title_up'] ?><br><span class="hc-text-regular hc-color-secondary"><?= get_field('project_benefits')['title_down'] ?></span></h2>
                    <div class="el-text-offset">
                        <i class="icon-arrow"></i>
                        <div class="el-cols">
                            <?php foreach (get_field('project_benefits')['items'] as $item): ?>
                                <div class="col formated-output">
                                    <h4 class="el-h4"><?= $item['title'] ?></h4>
                                    <p><?= $item['text'] ?></p>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $image_w_text = get_field('image_w_text')?>
        <div class="el-section section-item-1">
            <div class="inner">
                <div class="block-halves image-left">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2"><?= $image_w_text[0]['title']?></h2>
                        <div class="el-text-offset">
                            <?= $image_w_text[0]['text']?>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?= $image_w_text[0]['img']?>" class="upscale_1-5_x-5" >
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-2">
            <div class="inner">
                <div class="block-halves image-right">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2"><?= $image_w_text[1]['title']?></h2>
                        <div class="el-text-offset">
                            <?= $image_w_text[1]['text']?>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?= $image_w_text[1]['img']?>" class="upscale_1-5" >
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-item-3">
            <div class="inner">
                <div class="block-halves image-left">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2"><?= $image_w_text[2]['title']?></h2>
                        <div class="el-text-offset">
                            <?= $image_w_text[2]['text']?>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?= $image_w_text[2]['img']?>" class="upscale_1-5_x-20" >
                    </div>
                </div>
            </div>
        </div>

        <div class="el-section section-item-2">
            <div class="inner">
                <div class="block-halves image-right">
                    <div class="text-wrap formated-output">
                        <h2 class="el-h2"><?= $image_w_text[3]['title']?></h2>
                        <div class="el-text-offset">
							<p><?= $image_w_text[3]['text']?></p>
                            <a href="<?= get_field('link_na_wellness')['url'] ?>" target="<?= get_field('link_na_wellness')['target'] ?>" class="el-btn"><?= get_field('link_na_wellness')['title'] ?></a>
                        </div>
                    </div>
                    <div class="image-wrap">
                        <img src="<?= $image_w_text[3]['img']?>" class="" >
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part('templates/partials/section-contact') ?>
    </main>
<?php get_template_part('templates/partials/footer2') ?>
</div>
<?php get_template_part('templates/partials/footer') ?>
