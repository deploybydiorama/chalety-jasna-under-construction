<?php

use Tracy\Debugger;

require_once get_template_directory() . '/vendor/autoload.php';

// init
//Debugger::enable(['92.240.245.151'], __DIR__ . '/log');

// ACF setup
require_once get_template_directory() . '/includes/acf/settings.php';

//DISABLE GUTTENBERG
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);


/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                              PLUGINS SETUP                                  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

function dep18chalety_register_required_plugins(){

	// This is an example of how to include a plugin from the WordPress Plugin Repository.
	$plugins = [
		[
			'name'      => 'Advanced Custom Fields',
			'slug'      => 'advanced-custom-fields',
			'source' => get_template_directory() . '/plugins/acf.zip',
			'required'  => true,
			'force_activation' => true,
		],
        [
            'name'      => 'DrawAttention',
            'slug'      => 'draw-attention',
            'source' => get_template_directory() . '/plugins/draw-attention-pro-1.8.5.zip',
            'required'  => true,
            'force_activation' => true,
        ],
		[
			'name'      => 'Adminimize',
			'slug'      => 'adminimize',
			'required'  => true,
			'force_activation' => true,
		],
		[
			'name'      => 'Admin Menu Editor',
			'slug'      => 'admin-menu-editor',
			'required'  => true,
			'force_activation' => true,
		],
		[
			'name'      => 'All-in-One WP Migration',
			'slug'      => 'all-in-one-wp-migration',
			'required'  => true,
			'force_activation' => true,
		],
		[
			'name'      => 'Yoast SEO',
			'slug'      => 'wordpress-seo',
		],
//		[
//			'name'      => 'Loco Translate',
//			'slug'      => 'loco-translate',
//			'required'  => true,
//			'force_activation' => true,
//		],
//		[
//			'name'      => 'Polylang',
//			'slug'      => 'polylang',
//			'required'  => true,
//			'force_activation' => true,
//		],
		[
			'name'      => 'Contact Form 7',
			'slug'      => 'contact-form-7',
			'required'  => true,
			'force_activation' => true,
		],
        [
            'name'      => 'WP Mail SMTP',
            'slug'      => 'wp-mail-smtp',
            'required'  => true,
            'force_activation' => true,
        ],
		[
			'name'      => 'Flamingo',
			'slug'      => 'flamingo',
			'required'  => true,
			'force_activation' => true,
		],
		[
			'name'      => 'WP Migrate DB Pro',
			'slug'      => 'wp-migrate-db-pro',
            'source' => get_template_directory() . '/plugins/wp-migrate-db-pro-1.8.1.zip',
			'required'  => true,
			'force_activation' => true,
		],
		[
			'name'      => 'WP Migrate DB Pro CLI',
			'slug'      => 'wp-migrate-db-pro-cli:',
            'source' => get_template_directory() . '/plugins/wp-migrate-db-pro-cli-1.3.2.zip',
			'required'  => true,
			'force_activation' => true,
		],
		[
			'name'      => 'WP Migrate DB Pro Media Files',
			'slug'      => 'wp-migrate-db-pro-media-files',
            'source' => get_template_directory() . '/plugins/wp-migrate-db-pro-media-files-1.4.9.zip',
			'required'  => true,
			'force_activation' => true,
		],
	];

	$config = [
		'id'           => 'dep18chalety',  // Unique ID for hashing notices for multiple instances of TGMPA.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                   // If false, a user cannot dismiss the nag message.
		'is_automatic' => true,                    // Automatically activate plugins after installation or not.
	];

	tgmpa($plugins, $config);

}

add_action('tgmpa_register', 'dep18chalety_register_required_plugins');


/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                               THEME SETUP                                   *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function dep18chalety_setup() {
	/*
	* Make theme available for translation.
	* Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	* If you're building a theme based on Twenty Seventeen, use a find and replace
	* to change 'twentyseventeen' to the name of your theme in all the template files.
	*/
	load_theme_textdomain( 'dep18chalety' );

	/*
	* Let WordPress manage the document title.
	* By adding theme support, we declare that this theme does not use a
	* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
	* Enable support for Post Thumbnails on posts and pages.
	*
	* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	*/
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'dep18chalety-thumbnail', 1024, 1024, true );
	add_image_size( 'dep18chalety-gallery', 1920, 1080, true );
	add_image_size( 'dep18chalety-highlights', 960, 0, true );
    add_image_size( 'dep18chalety-developer-highlights', 1200, 0, true );
    add_image_size( 'dep18chalety-projekt-highlights', 1440, 0, true );
    add_image_size( 'dep18chalety-floorplan', 0, 810, true );
    add_image_size( 'dep18chalety-icon', 100, 0, true );

	// register menus
	register_nav_menus( array(
		'primary-menu' => __( 'Hlavné menu', 'dep18chalety' )
	) );

	// register user roles
	add_role('developer', 'Developer', get_role( 'administrator' )->capabilities);

}

add_action( 'after_setup_theme', 'dep18chalety_setup' );

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *                               ADMIN SETUP                                   *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
function dep18chalety_add_options_page() {

	if (!function_exists('acf_add_options_page')){
		return;
	}

	$settingsPage = acf_add_options_page(array(
		'page_title' => __('Nastavenia témy', 'dep18chalety'),
		'menu_title' => __('Nastavenia témy', 'dep18chalety'),
		'menu_slug'  => 'dep18chalety',
		'capability' => 'edit_posts',
		'autoload'   => true,
		'post_id'    => 'options',
		'redirect'   => false
	));

//	acf_add_options_sub_page(array(
//		'page_title'  => __('Podstránka'),
//		'menu_title'  => __('Podstránka', 'dep18chalety'),
//		'menu_slug'   => 'dep18chalety',
//		'capability'  => 'edit_posts',
//		'autoload'    => true,
//		'post_id'     => 'options-subpage',
//		'parent_slug' => $settingsPage['dep18chalety'],
//	));

}

add_action( 'after_setup_theme', 'dep18chalety_add_options_page' );
