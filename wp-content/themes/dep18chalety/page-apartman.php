<?php /* Template Name: Home */ ?>

<?php get_template_part('templates/partials/header') ?>
<body class="page-apartman">
<div id="main-wrapper">
    <?php get_template_part('templates/partials/header2') ?>
    <main id="main">
        <div class="el-section section-intro parallax-mover">
            <div class="el-float-obj num-1 parallax-mover-item-x" data-parallax-range-min="400" data-parallax-range="-600" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-26.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-26@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2 parallax-mover-item-x" data-parallax-range-min="150" data-parallax-range="-200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-17.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-17@2x.png 2x" style="width: 574px; height: 217px;" alt=""></div>
                <div class="block-text formated-output">
                    <h1 class="el-h1">detail apartmánu</h1>
                    <div class="el-text-offset hc-text-large">
                        <p>Luxusné apartmány ponúkame dokončené na kľúč s kompletne zariadenými interiérmi podľa
                            vizualizácií. Všetko v nadštandardnom prevedení a s použitím najkvalitnejších materiálov,
                            vrátane spotrebičov, sanity a doplnkov, pripravené na okamžité užívanie. V priestoroch
                            apartmánov budú použité predovšetkým prírodné materiály – to sa týka podláh,  obkladov stien
                            a stropov vo všetkých miestnostiach okrem dizajnovej kúpeľne a toaliet. V kuchynskej časti
                            bude stena upravená obkladom s kamenným efektom, rovnako aj časť stien v obývacej časti.
                            Nábytok bude ladený do nenápadných farieb v svetlých odtieňoch, vstavané skrine a kuchynka
                            vyhotovená z prírodnej dyhy. Celý priestor bude úplne presvetlený cez nadrozmerné okná
                            a terasu.</p>
                        <p>V súčasnosti sú k dispozícii vizualizácie chaletov a apartmánov, po postupnom dokončovaní je
                            možné dohodnúť sa na obhliadke a vyskúšať si svoj budúci apartmán.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="el-section section-visualization parallax-mover">
            <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="150" data-parallax-range="-300" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-29.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-29@2x.png 2x" alt=""></div>
            <div class="inner">
                <div class="el-float-obj num-2 parallax-mover-item" data-parallax-range-min="150" data-parallax-range="-300" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-36.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-36@2x.png 2x" alt=""></div>
                <div class="el-float-obj num-3 parallax-mover-item" data-parallax-range-min="400" data-parallax-range="-800" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-16.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-16@2x.png 2x" style="width: 254px; height: 280px;" alt=""></div>
                <div class="el-float-obj num-4 parallax-mover-item-x" data-parallax-range-min="150" data-parallax-range="-300" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-08.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-08@2x.png 2x" alt=""></div>

                <div class="section-3d-nav">
                    <div class="full-nav">
                        <h2 class="section-title el-h2 hc-color-primary">3d <span class="hc-text-light">vizualizácia</span></h2>
                        <?php // get_template_part('templates/partials/block-visualization') ?>
                        <div class="block-visualization">
                            <?php echo do_shortcode( '[drawattention]' ); ?>
                            <?php foreach (get_field('homes', 'options') as $index => $home): ?>
                                <?php /* Regroup apartments array by floors */ ?>
                                <?php $floors = []; ?>
                                <?php foreach ($home['apartments'] as $apartment): ?>
                                    <?php
                                        if(!isset($floors[$apartment['floor']])){
                                            $floors[$apartment['floor']] = [];
                                        }
                                        $floors[$apartment['floor']][] = $apartment;
                                    ?>
                                <?php endforeach ?>
                                <?php /* Create flyers */ ?>
                                <?php foreach ($floors as $floorId => $floorApartments): ?>
                                    <?php
                                        // check if all apartments are sold
                                        $floorSold = true;
                                        foreach ($floorApartments as $apartment){
                                            if ($apartment['ap_state']=='free'){
                                                $floorSold = false;
                                            }
                                        }
                                    ?>
                                    <div class="desc-wrap <?php if($floorSold): ?>desc-wrap--sold<?php endif ?>" data-id="chalet_<?= $home['id'] ?>_<?= $floorId ?>" >
                                        <h4 class="title">Chalet č.<?= $home['id'] ?></h4>
                                        <?php foreach ($floorApartments as $apartment): ?>
                                            <div class="apartment-label <?php if($apartment['ap_state']=='free'): ?>apartment-label--free<?php else:?>apartment-label--sold<?php endif ?>">Apartmán č. <?= $apartment['number'] ?></div>
                                        <?php endforeach ?>
                                        <a href="#podorys" class="btn"><?php if($apartment['ap_state']=='sold'): ?>Predané<?php elseif($apartment['ap_state']=='reserved'): ?>Rezervované<?php else: ?>Zobraziť<?php endif ?></a>
                                    </div>
                                <?php endforeach ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="select-nav" >
                        <h2 class="section-title el-h2 hc-color-primary">Výber <span class="hc-text-light">podlažia</span></h2>
                        <div class="select-wrap">
                            <select>
                                <?php foreach (get_field('homes', 'options') as $index => $home): ?>
                                    <?php /* Regroup apartments array by floors */ ?>
                                    <?php $floors = []; ?>
                                    <?php foreach ($home['apartments'] as $apartment): ?>
                                        <?php
                                        if(!isset($floors[$apartment['floor']])){
                                            $floors[$apartment['floor']] = [];
                                        }
                                        $floors[$apartment['floor']][] = $apartment;
                                        ?>
                                    <?php endforeach ?>
                                    <?php /* Create options */ ?>
                                    <?php foreach ($floors as $floorId => $floorApartments): ?>
                                        <option value="chalet_<?= $home['id'] ?>_<?= $floorId ?>" >
                                            <?= $home['name'] ?>: <?= $floorApartments[0]['floor_name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="block-plan">
                    <a name="podorys"></a>
                    <?php foreach (get_field('homes', 'options') as $index => $home): ?>
                        <?php foreach ($home['apartments'] as $apartment): ?>
                            <div class="plan-wrap <?php if($index == 0 and $apartment['floor'] == 1): ?>active<?php endif ?>" data-id="chalet_<?= $home['id'] ?>_<?= $apartment['floor'] ?>" >
                                <div class="image-wrap">
                                    <img src="<?= $apartment['floorplan'] ?>" >
                                </div>
                                <div class="text-wrap">
                                    <h3 class="title el-h3">Chalet <strong>č. <?= $home['id'] ?></strong></h3>
                                    <ul class="params">
                                        <li><div class="<?php if($apartment['ap_state']=='free'): ?>apartment-label--free<?php else:?>apartment-label--sold<?php endif ?> apartment-label">Apartmán : <strong><?= $apartment['number'] ?></strong></div></li>
                                        <li>Typ: <strong><?= $apartment['type'] ?></strong></li>
                                        <li>Poschodie: <strong><?= $apartment['floor_name'] ?></strong></li>
                                        <li>Výmera: <strong><?= $apartment['terrace_area'] + $apartment['hall_area'] + $apartment['room_area'] + $apartment['living_area'] + $apartment['bedroom_area'] + $apartment['hygiene_area'] ?> m<sup>2</sup></strong></li>
                                        <?php if(!empty($apartment['terrace_area'])): ?>
                                            <li>Z toho terasa: <strong><?= $apartment['terrace_area'] ?> m<sup>2</sup></strong></li>
                                        <?php endif ?>
                                        <?php if(!empty($apartment['living_area'])): ?>
                                            <li>Obytný priestor: <strong><?= $apartment['living_area'] ?> m<sup>2</sup></strong></li>
                                        <?php endif ?>
                                        <?php if(!empty($apartment['hall_area'])): ?>
                                            <li>Predsieň: <strong><?= $apartment['hall_area'] ?> m<sup>2</sup></strong></li>
                                        <?php endif ?>
                                        <?php if(!empty($apartment['room_area'])): ?>
                                            <li>Izba samostatná: <strong><?= $apartment['room_area'] ?> m<sup>2</sup></strong></li>
                                        <?php endif ?>
                                        <?php if(!empty($apartment['bedroom_area'])): ?>
                                            <li>Spálňa samostatná: <strong><?= $apartment['bedroom_area'] ?> m<sup>2</sup></strong></li>
                                        <?php endif ?>
                                        <?php if(!empty($apartment['hygiene_area'])): ?>
                                            <li>Hygienické zázemie: <strong><?= $apartment['hygiene_area'] ?> m<sup>2</sup></strong></li>
                                        <?php endif ?>
                                        <li>Orientácia spálne: <strong><?= $apartment['bedroom_direction'] ?></strong></li>
                                        <?php if(!empty($apartment['terrace_direction'])): ?>
                                            <li>Orientácia terasy: <strong><?= $apartment['terrace_direction'] ?></strong></li>
                                        <?php endif ?>
                                        <?php if(!empty($apartment['ap_state'])): ?>
                                            <li><a href="/" class="button <?php if($apartment['ap_state']=='free'): ?>button--free<?php else:?>button--sold<?php endif ?>">STAV: <strong><?php if($apartment['ap_state']=='free'): ?>voľný<?php elseif($apartment['ap_state']=='reserved'): ?>Rezervovaný<?php else:?>predaný<?php endif ?></strong></a></li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endforeach ?>
                    <?php if(0): ?>
                    <div class="legend-wrap">
                        <h3 class="title">Legenda</h3>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-1"></span><span>Prenajímateľná plocha</span></div>
                            <div class="legend-col"><span>323,12m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-2"></span><span>Hygienické zázemie</span></div>
                            <div class="legend-col"><span>26,44m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-3"></span><span>Spoločné priestory</span></div>
                            <div class="legend-col"><span>15,97m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-4"></span><span>Vertikálne komunikácie</span></div>
                            <div class="legend-col"><span>42,37m<sup>2</sup></span></div>
                        </div>
                        <div class="legend-row">
                            <div class="legend-col"><span class="el-legend-color color-5"></span><span>Terasa</span></div>
                            <div class="legend-col"><span>117,80m<sup>2</sup></span></div>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <?php get_template_part('templates/partials/section-contact') ?>
    </main>
<?php get_template_part('templates/partials/footer2') ?>
</div>
<?php get_template_part('templates/partials/footer') ?>