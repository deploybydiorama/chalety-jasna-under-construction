<footer id="footer" class="parallax-mover-x" >
    <div class="el-float-obj num-1 parallax-mover-item" data-parallax-range-min="300" data-parallax-range="-300" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-22.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-22@2x.png 2x" style="width: 420px; height: 506px;" alt=""></div>
    <div class="inner">
        <div class="footer-cols">
            <div class="footer-col">
                <h3 class="footer-title">Navigácia:</h3>
                <?=
                wp_nav_menu([
                    'container' => false,
                    'menu_class' => 'footer-menu',
                    'theme_location' => 'primary-menu'
                ]);
                ?>
            </div>
            <div class="footer-col">
                <?php if(get_field('facebook_url', 'options') or
                         get_field('instagram_url', 'options') or
                         get_field('youtube_url', 'options')): ?>
                <h3 class="footer-title">Navigácia:</h3>
                <?php endif ?>
                <ul class="footer-social">
                    <?php if(get_field('facebook_url', 'options')): ?>
                        <li><a href="<?= get_field('facebook_url', 'options') ?>" target="_blank" ><i class="icon-facebook"></i></a></li>
                    <?php endif ?>
                    <?php if(get_field('instagram_url', 'options')): ?>
                        <li><a href="<?= get_field('instagram_url', 'options') ?>" target="_blank" ><i class="icon-instagram"></i></a></li>
                    <?php endif ?>
                    <?php if(get_field('youtube_url', 'options')): ?>
                        <li><a href="<?= get_field('youtube_url', 'options') ?>" target="_blank" ><i class="icon-youtube"></i></a></li>
                    <?php endif ?>
                </ul>
            </div>
            <div class="footer-col">
                <h3 class="footer-title">Investor:</h3>
                <div class="footer-text formated-output">
                    <p>
                        <?= get_field('address', 'options') ?>
                        <?php if(get_field('email', 'options')): ?>
                        <a href="mailto:<?= get_field('email', 'options') ?>"><?= get_field('email', 'options') ?></a>
                        <?php endif ?>
                        <br>
                        <a href="tel:<?= preg_replace('/[^\+\d]/', '', get_field('phone', 'options')) ?>" ><?= get_field('phone', 'options') ?></a>
                    </p>
                </div>
            </div>
            <div class="footer-col">
                <h3 class="footer-title">Autori projektu:</h3>
                <ul class="footer-menu">
                    <?php if(get_field('homepage_url', 'options')): ?>
                    <li><a href="<?= get_field('homepage_url', 'options') ?>" target="_blank" >Airavata</a></li>
                    <?php endif ?>
                    <?php if(get_field('author_builder_url', 'options')): ?>
                        <li><a href="<?= get_field('author_builder_url', 'options') ?>" target="_blank" >Stavebná spoločnosť</a></li>
                    <?php endif ?>
                    <?php if(get_field('author_architect_url', 'options')): ?>
                        <li><a href="<?= get_field('author_architect_url', 'options') ?>" target="_blank" >Architekti</a></li>
                    <?php endif ?>
                </ul>
            </div>
        </div>
    </div>
</footer>