<header id="header" >
    <div class="inner" >
        <a href="<?= get_home_url() ?>" class="logo"><span class="logo-title"><span class="hc-color-white">Chalety</span><br>lúčky</span><br><span class="logo-slogan"><span class="slogan-first">predaj<br>apartmánov</span><br><span class="slogan-second">v srdci prírody</span></span></a>
        <a class="nav-toggle"><span class="burger"></span></a>

        <nav class="header-nav">
            <?=
                wp_nav_menu([
                    'container' => false,
                    'menu_class' => 'header-menu',
                    'theme_location' => 'primary-menu'
                ]);
            ?>
            <ul class="header-social">
                <?php if(get_field('facebook_url', 'options')): ?>
                    <li><a href="<?= get_field('facebook_url', 'options') ?>" target="_blank" ><i class="icon-facebook"></i></a></li>
                <?php endif ?>
                <?php if(get_field('instagram_url', 'options')): ?>
                    <li><a href="<?= get_field('instagram_url', 'options') ?>" target="_blank" ><i class="icon-instagram"></i></a></li>
                <?php endif ?>
                <li><a href="tel:<?= preg_replace('/[^\+\d]/', '', get_field('phone', 'options')) ?>" ><i class="icon-phone"></i></a></li>
            </ul>
        </nav>
    </div>
</header>