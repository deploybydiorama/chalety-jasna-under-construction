<div class="el-section section-contact">
    <a name="kontakt"></a>
    <div class="inner">
        <div class="el-float-obj num-1" data-parallax-range-min="200" data-parallax-range="-200" ><img src="<?php echo get_template_directory_uri(); ?>/public/i/fo-21.png" srcset="<?php echo get_template_directory_uri(); ?>/public/i/fo-21@2x.png 2x" alt=""></div>
        <div class="block-text">
            <h2 class="section-title el-h2">zaujala vás naša ponuka?</h2>
            <a href="tel:<?= preg_replace('/[^\+\d]/', '', get_field('phone', 'options')) ?>" class="cta" >
                <i class="icon-phone"></i> volajte na <span class="phone-number"><?= get_field('phone', 'options') ?></span>
            </a>
        </div>
        <div class="block-text ">
            <div class="download-link">
                alebo si nechajte zaslať
                <span class="download-link__link" >cenník na vyžiadanie</span>
            </div>
        </div>
        <div class="request-quote">
            <button class="request-quote__close"></button>
            <h1 class="request-quote__heading">Cenník na vyžiadanie</h1>
            <?= do_shortcode('[contact-form-7 id="290" title="Cenník na vyžiadanie"]') ?>
            <?php if(0): ?>
            <div class="form_holder">
                <div class="input_holder">
                        <input type="text" class="request-quote__input" placeholder="meno"/>
                </div>
                <div class="input_holder">
                    <div class="input_holder__control">
                        <input type="tel" class="request-quote__input" placeholder="telefón"/>
                    </div>
                    <div class="input_holder__control">
                        <input type="text" class="request-quote__input" placeholder="email"/>
                    </div>
                </div>
                <textarea class="request-quote__textarea" placeholder="text správy"></textarea>
                <button class="request-quote__button" type="submit">Odoslať</button>
            </div>
            <?php endif ?>
        </div>
    </div>
</div>