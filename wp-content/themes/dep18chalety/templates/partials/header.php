<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- META -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116300085-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116300085-1');
    </script>

    <!-- WP_HEAD -->
    <?php wp_head(); ?>
    <!-- END WP_HEAD -->

    <!-- TODO -->
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/public/i/favicon.png" type="image/png">

    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, target-densityDpi=160dpi">
    <meta http-equiv="cleartype" content="on">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- TODO -->
    <link href="<?= get_stylesheet_directory_uri() ?>/public/css/main.min.css?<?= @filemtime(get_stylesheet_directory() . '/public/css/main.min.css') ?>" rel="stylesheet" type="text/css">

    <!-- script for loading page cookie -->
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

</head>
<body>


<?php if (get_field( 'facebook_app_id' , 'options')): ?>
<!-- Facebook JS SDK -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : "<?= get_field( 'facebook_app_id' , 'options') ?>",
            xfbml      : true,
            status     : true,
            cookie     : true,
            version    : "v2.7"
        });

        // trigger load event
        document.documentElement.className += ' facebook';
        if (typeof jQuery !== 'undefined'){
            jQuery(window).trigger('facebook');
        }
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/" + "sk_SK" + "/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- /Facebook JS SDK -->

<?php endif ?>

