module.exports = function (grunt) {

    // autoloads npm tasks
    require('load-grunt-tasks')(grunt);

    // helpers
    Helpers = {
        slug: function (str, leaveCase) {

            str = str.replace(/^\s+|\s+$/g, ''); // trim
            if (leaveCase !== true){
                str = str.toLowerCase();
            }

            // remove accents, swap ñ for n, etc
            var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
            var to   = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^A-Za-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;

        }
    };

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // *************** INSTALL ****************
        prompt: {
            name: {
                options: {
                    questions: [
                        {
                            config: 'prompt.name',
                            type: 'input',
                            message: 'What is the name of this project?',
                            validate: function(value){ return /[a-zA-Z0-9][a-zA-Z0-9-_ ]{2,}/.test(); }
                        }
                    ]
                }
            }
        },
        replace: {
            nameUndesrcore: {
                src: ['wp-content/themes/my-theme/*.*', 'wp-content/themes/my-theme/includes/**/*.*'],
                overwrite: true,
                replacements: [{
                    from: 'my_theme',
                    to: function(){ return Helpers.slug(grunt.config('prompt.name')).replace(/-/g, '_'); }
                }]
            },
            nameDash: {
                src: ['theme.bat', 'wp-content/themes/my-theme/*.*', 'wp-content/themes/my-theme/includes/**/*.*'],
                overwrite: true,
                replacements: [{
                    from: 'my-theme',
                    to: function(){ return Helpers.slug(grunt.config('prompt.name')); }
                }]
            },
            nameUndesrcoreUppercase: {
                src: ['wp-content/themes/my-theme/*.*', 'wp-content/themes/my-theme/includes/**/*.*'],
                overwrite: true,
                replacements: [{
                    from: 'My_Theme',
                    to: function(){ return Helpers.slug(grunt.config('prompt.name'), true).replace(/-/g, '_'); }
                }]
            },
            nameWords: {
                src: ['wp-content/themes/my-theme/*.*', 'wp-content/themes/my-theme/includes/**/*.*'],
                overwrite: true,
                replacements: [{
                    from: 'My Theme',
                    to: function(){ return grunt.config('prompt.name'); }
                }]
            }
        },
        rename: {
            folder: {
                files: [
                    {
                        expand: true,
                        src: 'wp-content/themes/my-theme',
                        rename: function(){
                            return 'wp-content/themes/' + Helpers.slug(grunt.config('prompt.name'));
                        }
                    }
                ]
            }
        }
    });

    // register tasks
    grunt.registerTask('install', ['prompt', 'replace', 'rename']);
    grunt.registerTask('default', []);

};